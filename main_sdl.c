/*
SDL2 frontend for the game

Licensed under CC0, public domain, 2023-2024
Made by blitzdoughnuts
*/

#include <stdio.h>
#include <stdint.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#undef main

// DEBUG
// #define WIDTH 480
// #define HEIGHT 270

#include "game.h" // keep in mind, MAIN can still access variables from the game!

#ifndef EDITMODE
#define EDITMODE 0
#endif

#ifndef WTH_SDL_DEMOSUPPORT
#define WTH_SDL_DEMOSUPPORT 0
#endif

// for aspect correction
#define CAM_VERTOFFSET ((HEIGHT - 270) / 4)

#if EDITMODE == 1
uint8_t editkeys;
uint8_t editkeys_prev;
#endif
// the hackiest debug feature TO DATE

// lookup table for font offsets, to offset the character afterwards to the left by this amount
static const uint8_t fontOffsets_left[90] = {
	21, 12, 3, 11, 7, 14, 22, 20, 20, 4, 4, 21, 1, 20, 11, // 33-47
	8, 10, 6, 11, 10, 8, 9, 6, 13, 7, 5, 23, 21, 5, 24, 6, 24, // 48-64
	0, 7, 7, 7, 4, 7, 4, 0, 0, 0, 9, 7, 0, 9, 4, 9, 5, 11, 9, 6, 6, 10, 3, 3, 9, 5, // 64-96 (A-Z)
	12, 12, 12, 16, 3, 21, // 97-102
	14, 16, 13, 16, 14, 15, 12, 11, 23, // 103-122 (a-z)
	18, 18, 21, 3, 12, 12, 15, 10, 12, 13, 13, 13, 15, 5, 13, 13, 13 // 123-139
};

// strings here, for both ease of access and also to reduce redundancy in program string lists
static const char str_on[] = "ON";
static const char str_off[] = "OFF";
static const char str_exit[] = "Exit";
static const char str_savegamename[] = "WTH_SaveGame.bin";

#ifndef WTHOPTS_SDL_NOMIXER
#include <SDL2/SDL_mixer.h>

Mix_Music *music[3];
Mix_Chunk *sfx[12];
#endif

//SDL_Surface *winsurf;
SDL_Texture *wintext;

SDL_Texture *plrsprites[10];
SDL_Texture *sprites[20];

SDL_Rect dspdest_rect; // this is the DESTINATION, aka screen position
SDL_Rect fade_rect; // used for fading in/out

SDL_Point zeropoint = {0,0};

SDL_Renderer *render;
SDL_Window *win;

uint8_t running;

void drawSprite(SDL_Texture *sprite, int x, int y) {
	dspdest_rect = (SDL_Rect){x, y, 0, 0};
	SDL_QueryTexture(sprite, NULL, NULL, &dspdest_rect.w, &dspdest_rect.h);
	SDL_RenderCopy(render, sprite, NULL, &dspdest_rect);
};

FILE *DaSave;

#if WTH_SDL_DEMOSUPPORT == 1
FILE *DaDemo;
uint8_t demoStatus;
uint8_t demowrite[1];
uint32_t demoPointer;
static const char str_wintitle[] = WINTITLE; // it's only here to cut down redundancies
#endif

void drawRepeatingSprite(SDL_Texture *sprite, int x, int y) {
	int _x = x;
	dspdest_rect.x = x;
	dspdest_rect.y = y;
	SDL_QueryTexture(sprite, NULL, NULL, &dspdest_rect.w, &dspdest_rect.h);
	while (_x < WIDTH) {
		if (_x < -dspdest_rect.w) {
			_x += dspdest_rect.w;
			dspdest_rect.x = _x;
			continue;
		};
		SDL_RenderCopy(render, sprite, NULL, &dspdest_rect);
		_x += dspdest_rect.w;
		dspdest_rect.x = _x;
	};
	_x = x;
	while (_x > -dspdest_rect.w) {
		if (_x > WIDTH) {
			_x -= dspdest_rect.w;
			dspdest_rect.x = _x;
			continue;
		};
		_x -= dspdest_rect.w;
		SDL_RenderCopy(render, sprite, NULL, &dspdest_rect);
		dspdest_rect.x = _x;
	};
};

void drawSpriteSheeted(SDL_Texture *sprite, int x, int y, int frame, int x_sprdist, int y_sprdist, uint8_t flipped) { // supports only purely horizontal sheets
	if (x < -x_sprdist || x > x_sprdist + WIDTH) return;
	SDL_Rect destrect = {x, y, x_sprdist, y_sprdist};
	dspdest_rect.x = x_sprdist * frame;
	dspdest_rect.y = 0; // this somehow fixes an issue where the sprite would vertically stretch when moving the camera vertically, but crop off within the sprite's intended bounds. what?
	dspdest_rect.w = x_sprdist,
	dspdest_rect.h = y_sprdist,
	SDL_RenderCopyEx(render, sprite, &dspdest_rect, &destrect, 0, &zeropoint, flipped ? SDL_FLIP_HORIZONTAL : SDL_FLIP_NONE);
};

void drawTextString(const char *stuff, int x, int y, uint8_t color) {
	// to-do: may need optimizing?
	switch (color)
	{
		case 1: SDL_SetTextureColorMod(sprites[WTH_SPR_FONTMAP], 224, 0, 0); break;/*
		case 2: SDL_SetTextureColorMod(sprites[5], 0, 224, 21); break;
		case 3: SDL_SetTextureColorMod(sprites[5], 0, 31, 224); break;
		case 4: SDL_SetTextureColorMod(sprites[5], 255, 255, 0); break;
		case 5: SDL_SetTextureColorMod(sprites[5], 0, 255, 255); break;
		case 6: SDL_SetTextureColorMod(sprites[5], 255, 0, 255); break;
		case 7: SDL_SetTextureColorMod(sprites[5], 0, 255, 255); break;
		case 8: SDL_SetTextureColorMod(sprites[5], 255, 255, 0); break;
		case 9: SDL_SetTextureColorMod(sprites[5], 255, 255, 255); break;
		case 10: SDL_SetTextureColorMod(sprites[5], 0, 0, 0); break;*/
		// these extra colors ain't used
		default: SDL_SetTextureColorMod(sprites[WTH_SPR_FONTMAP], 218, 218, 218); break;
	}
	int16_t _x = x;
        for (uint8_t i = 0; i < 255; i++) {
	        if (stuff[i] == '\0') break; // terminator character? then get outta there before stuff gets nasty
		if (_x < -27 || _x > 27 + WIDTH) return;
		SDL_Rect destrect = {_x, y, 27, 30};
		dspdest_rect.x = 27 * (stuff[i] - 33);
		dspdest_rect.y = 0;
		dspdest_rect.w = 27;
		dspdest_rect.h = 30;
		SDL_RenderCopy(render, sprites[WTH_SPR_FONTMAP], &dspdest_rect, &destrect);
	        _x += (stuff[i] == 32) ? 16 : 27 - fontOffsets_left[stuff[i] - 33];
        };
};

void signalMisc(uint8_t signal) {
    switch (signal)
    {
        case MISC_PAUSEMUSIC: case MISC_RESUMEMUSIC: case MISC_STOPMUSIC:
            if (!(options[0] & WTHOPTS_DOSONG)) break;
            switch (signal)
            {
                case MISC_PAUSEMUSIC: Mix_PauseMusic(); break;
                case MISC_RESUMEMUSIC: Mix_ResumeMusic(); break;
                case MISC_STOPMUSIC: Mix_HaltMusic(); break;
            }
            break;
        case MISC_EXITGAME:
	    	running = 0;
        	break;
    }
};

#define WTH_OPTIONTEXT_X 30
#define WTH_OPTIONTEXT_Y 40

void drawOptions() {
	// drawRepeatingSprite has no functionality for VERTICAL tiling... fuck it, we ball
	drawRepeatingSprite(sprites[WTH_SPR_OPTMENU], 0, 0);
	drawRepeatingSprite(sprites[WTH_SPR_OPTMENU], 0, 64);
	drawRepeatingSprite(sprites[WTH_SPR_OPTMENU], 0, 128);
	drawRepeatingSprite(sprites[WTH_SPR_OPTMENU], 0, 192);
	drawRepeatingSprite(sprites[WTH_SPR_OPTMENU], 0, 254);
	drawTextString("Fade", WTH_OPTIONTEXT_X,WTH_OPTIONTEXT_Y, menu.menuselect == 0 ? 1 : 0);
	drawTextString((options[0] & WTHOPTS_NOFADE) ? str_off : str_on, 320, WTH_OPTIONTEXT_Y, menu.menuselect == 0 ? 1 : 0);
	drawTextString("Freecam", WTH_OPTIONTEXT_X,WTH_OPTIONTEXT_Y + 32, menu.menuselect == 1 ? 1 : 0);
	drawTextString((options[0] & WTHOPTS_DEVCAM) ? str_on : str_off, 320, WTH_OPTIONTEXT_Y + 32, menu.menuselect == 1 ? 1 : 0);
	drawTextString("Music", WTH_OPTIONTEXT_X,WTH_OPTIONTEXT_Y + 64, menu.menuselect == 2 ? 1 : 0);
	drawTextString((options[0] & WTHOPTS_DOSONG) ? str_on : str_off, 320, WTH_OPTIONTEXT_Y + 64, menu.menuselect == 2 ? 1 : 0);
	drawTextString("Double Res", WTH_OPTIONTEXT_X,WTH_OPTIONTEXT_Y + 96, menu.menuselect == 3 ? 1 : 0);
	drawTextString((options[0] & WTHOPTS_2XSIZE) ? "YES" : "NO", 320, WTH_OPTIONTEXT_Y + 96, menu.menuselect == 3 ? 1 : 0);
	drawTextString("Fullscreen", WTH_OPTIONTEXT_X,WTH_OPTIONTEXT_Y + 128, menu.menuselect == 4 ? 1 : 0);
	drawTextString((options[0] & WTHOPTS_FULLSC) ? str_on : str_off, 320, WTH_OPTIONTEXT_Y + 128, menu.menuselect == 4 ? 1 : 0);
	
	drawTextString(str_exit, 20,238, menu.menuselect == 5 ? 1 : 0);
};

void signalDraw(uint8_t index) {
	switch (index)
	{
		case DRAW_FADEIN: case DRAW_FADEOUT: case DRAW_HALTFADE:
			if (options[0] & WTHOPTS_NOFADE) break;
			switch (index)
			{
				case DRAW_FADEIN:
					fade = 255;
					fademode = 1;
					SDL_SetRenderDrawBlendMode(render, SDL_BLENDMODE_BLEND);
					break;
				case DRAW_FADEOUT:
					fade = 0;
					fademode = 2;
					SDL_SetRenderDrawBlendMode(render, SDL_BLENDMODE_BLEND);
					break;
				case DRAW_HALTFADE:
					fade = 0;
					fademode = 0;
					SDL_SetRenderDrawBlendMode(render, SDL_BLENDMODE_NONE);
					break;
			};
			break;
		case DRAW_PLRUPARROW:
         		drawSprite(sprites[WTH_SPR_UPARROW], plr.x - 20 - cam.x, plr.y - 150 - cam.y);
           		break;
		case DRAW_CHECK2XSIZE:
			if (options[0] & WTHOPTS_2XSIZE)
				SDL_SetWindowSize(win, WIDTH * 2, HEIGHT * 2);
			else
				SDL_SetWindowSize(win, WIDTH, HEIGHT);
			SDL_SetWindowPosition(win, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);
			break;
		case DRAW_CHECKFULLSC:
			SDL_SetWindowFullscreen(win, (options[0] & WTHOPTS_FULLSC) ? SDL_WINDOW_FULLSCREEN_DESKTOP : 0);
			break;
	};
};

// inline the simple stuff
static inline void signalPlaySFX(uint8_t signal) {
	#ifndef WTHOPTS_SDL_NOMIXER
	Mix_PlayChannel(-1, sfx[signal], 0);
	#endif
};

static inline void signalPlayMUS(uint8_t signal) {
	#ifndef WTHOPTS_SDL_NOMIXER
	if (options[0] & WTHOPTS_DOSONG) Mix_PlayMusic(music[signal], -1);
	#endif
};

void saveGame() {
	DaSave = fopen(str_savegamename,"w");
	fwrite(&options, 1, 2, DaSave);
	fclose(DaSave);
};

void loadGame() {
	if (!(DaSave = fopen(str_savegamename,"r"))) {
		options[0] = DEFAULT_OPTIONS;
		return;
	};
	fread(&options, 1, 2, DaSave);
	fclose(DaSave);
};

void draw() {
	switch (GAME_STATE)
	{
		case GSTATE_MENU:
			//SDL_FillRect(winsurf, NULL, 400);
			switch (menu.menuindex)
			{
				case 0:
					drawSprite(sprites[WTH_SPR_MENU], 0, 0);
					drawSpriteSheeted(sprites[WTH_SPR_MENUBUTTONS], 47, 147, (menu.menuselect == 0) ? 1 : 0, 99, 23, 0);
					drawSpriteSheeted(sprites[WTH_SPR_MENUBUTTONS], 47, 170, (menu.menuselect == 1) ? 3 : 2, 99, 23, 0);
					drawSpriteSheeted(sprites[WTH_SPR_MENUBUTTONS], 47, 193, (menu.menuselect == 2) ? 5 : 4, 99, 23, 0);
					break;
				case 1: case MINDEX_XOPTIONS:
					drawOptions();
					break;
			}
			drawTextString(VERSION_NUMBER, 0, 0, 0);
			
			break;
		case GSTATE_PLAYING:
			//SDL_FillRect(winsurf, NULL, 400);
			switch (level)
			{
				case ROOM_TEST: case ROOM_STREETS: case ROOM_H_ENT:
					drawRepeatingSprite(sprites[WTH_SPR_BG_STREET], -cam.x, -cam.y);
					break;
				case ROOM_HOUSE:
					drawSprite(sprites[WTH_SPR_BG_HOUSE], -90 - cam.x - 75, -cam.y);
					break;
				case ROOM_OUTDOORS:
					drawRepeatingSprite(sprites[WTH_SPR_BG_OUTDOORS], -cam.x / 2, -cam.y);
					break;
				case ROOM_HOSPITAL:
					drawSprite(sprites[WTH_SPR_BG_HOSPITAL], -45 - cam.x, -cam.y);
					break;
			};
			
			for (uint8_t i = 0; i < interacts_count; i++) {
				if (!(interacts[i].flags & INTER_ACTIVE)) continue;
				switch (interacts[i].objID)
				{
					case 255: 
						drawSpriteSheeted(sprites[WTH_SPR_YOU], (interacts[i].x - 75) - cam.x, (interacts[i].y - 100) - cam.y, interacts[i].vars[1], 150, 150, (plr.x < interacts[i].x) ? 1 : 0);
						break;
					case INTERTYPE_COAT:
						drawSpriteSheeted(sprites[WTH_SPR_COATRACK], interacts[i].x - 35 - cam.x, (interacts[i].y - 100) - cam.y, (plr.flags & FLAG_HASCOAT) ? 1 : 0, 70, 100, 0);
						break;
					case INTERTYPE_DECOR:
						drawSprite(sprites[interacts[i].vars[0]], interacts[i].x - cam.x, interacts[i].y - cam.y);
						break;
					case INTERTYPE_ARTIFACT:
						drawSpriteSheeted(sprites[WTH_SPR_ARTIFACTS], (interacts[i].x - 32) - cam.x, (interacts[i].y - 32) - cam.y, interacts[i].vars[0], 64, 64, 0);
						break;
					case INTERTYPE_DOOR:
						drawSpriteSheeted(sprites[WTH_SPR_DOOR], (interacts[i].x - 75) - cam.x, (interacts[i].y - 100) - cam.y, ((interacts[i].flags & 1 << 2) || interacts[i].vars[1]) ? 1 : 0, 150, 150, 0);
						break;
					case INTERTYPE_STAIRS:
						drawSprite(sprites[WTH_SPR_DOOR], (interacts[i].x - 75) - cam.x, (interacts[i].y - 100) - cam.y);
						break;
					case INTERTYPE_BOOK:
						drawSpriteSheeted(sprites[WTH_SPR_BOOK_HAMIE], (interacts[i].x - 32) - cam.x, (interacts[i].y - 32) - cam.y, ((interacts[i].flags & 1 << 2) || interacts[i].vars[1]) ? 1 : 0, 64, 64, 0);
						break;
				}
			};
			
			drawSpriteSheeted(plrsprites[(plr.flags & FLAG_HASCOAT) ? plr.animindex : plr.animindex + 5], (plr.x - 75) - cam.x, (plr.y - 100) - cam.y, plr.animframe, 150, 150, (plr.flags & FLAG_FLIPPED) ? 1 : 0);
			
			#ifdef WTHCOMPILER_DEBUG
			switch (level)
			{
				case 0: drawTextString("Test Room", 0, 0, 0); break;
				case 1: drawTextString("House", 0, 0, 0); break;
				case 2: drawTextString("Outside of House", 0, 0, 0); break;
				case 3: drawTextString("Endless Sidewalk", 0, 0, 0); break;
				case 4: drawTextString("Hospital Entrance", 0, 0, 0); break;
				case 5: drawTextString("Hospital Lobby", 0, 0, 0); break;
				case 11: drawTextString("Bar", 0, 0, 0); break;
			};
			#endif

			if (fade != 0) {
				SDL_SetRenderDrawColor(render, 0,0,0, fade);
				SDL_RenderFillRect(render, NULL);
			};
			break;
		case GSTATE_PAUSED:
			//SDL_FillRect(winsurf, NULL, 400);
			if (plr.artifacts & 1 << ARTIFACT_BADGE) {
				drawSpriteSheeted(sprites[WTH_SPR_ARTIFACTS], 0, 0, 0, 64, 64, 0);
			};
			if (plr.artifacts & 1 << ARTIFACT_MIRROR)
				drawSpriteSheeted(sprites[WTH_SPR_ARTIFACTS], 64, 0, 1, 64, 64, 0);
			if (plr.artifacts & 1 << ARTIFACT_DONUT)
				drawSpriteSheeted(sprites[WTH_SPR_ARTIFACTS], 128, 0, 2, 64, 64, 0);
			if (plr.artifacts & 1 << ARTIFACT_KNIFE)
				drawSpriteSheeted(sprites[WTH_SPR_ARTIFACTS], 192, 0, 3, 64, 64, 0);
			switch (menu.menuindex)
			{
				case 0:
					drawTextString("PAUSED",200, 75, 0);
					drawTextString("Resume", 200,120, menu.menuselect == 0 ? 1 : 0);
					drawTextString("Options", 200,152, menu.menuselect == 1 ? 1 : 0);
					drawTextString(str_exit, 200,184, menu.menuselect == 2 ? 1 : 0);
					break;
				case 1:
					drawOptions();
					break;
			}
			break;
		case GSTATE_BOOK:
			drawSpriteSheeted(sprites[WTH_SPR_READ_HAMIE], 0, 0, bookpage & 0x0F, 480, 270, 0);
			if (fade != 0) {
				SDL_SetRenderDrawColor(render, 0,0,0, fade);
				SDL_RenderFillRect(render, NULL);
			};
			break;
	};
};

uint8_t *keystates;
SDL_Joystick *controllerinput;

void keys() {
    #if WTH_SDL_DEMOSUPPORT == 1
    if (demoStatus != 2) {
    #endif
	if (keystates[SDL_SCANCODE_LEFT]) input_keys |= KEY_LEFT;
	if (keystates[SDL_SCANCODE_RIGHT]) input_keys |= KEY_RIGHT;
	if (keystates[SDL_SCANCODE_UP]) input_keys |= KEY_UP;
	if (keystates[SDL_SCANCODE_DOWN]) input_keys |= KEY_DOWN;
	if (keystates[SDL_SCANCODE_Z]) input_keys |= KEY_GESTURE;
	if (keystates[SDL_SCANCODE_ESCAPE]) input_keys |= KEY_MENU;
    #if WTH_SDL_DEMOSUPPORT == 1
    };
    #endif
	
	if (keystates[SDL_SCANCODE_A]) xtrakeys |= KEY_LEFT;
	if (keystates[SDL_SCANCODE_D]) xtrakeys |= KEY_RIGHT;
	if (keystates[SDL_SCANCODE_W]) xtrakeys |= KEY_UP;
	if (keystates[SDL_SCANCODE_S]) xtrakeys |= KEY_DOWN;
	if (keystates[SDL_SCANCODE_X]) xtrakeys |= KEY_ATTACK;
	
	if (controllerinput) {
	    printf("Registering controller input...\n");
		#if WTH_SDL_DEMOSUPPORT == 1
		if (demoStatus != 2) {
		#endif
	    if (SDL_JoystickGetButton(controllerinput, SDL_CONTROLLER_BUTTON_DPAD_LEFT) || SDL_JoystickGetAxis(controllerinput, SDL_CONTROLLER_AXIS_LEFTX) < -16383) input_keys |= KEY_LEFT;
	    if (SDL_JoystickGetButton(controllerinput, SDL_CONTROLLER_BUTTON_DPAD_RIGHT) || SDL_JoystickGetAxis(controllerinput, SDL_CONTROLLER_AXIS_LEFTX) > 16383) input_keys |= KEY_RIGHT;
	    if (SDL_JoystickGetButton(controllerinput, SDL_CONTROLLER_BUTTON_DPAD_UP) || SDL_JoystickGetAxis(controllerinput, SDL_CONTROLLER_AXIS_LEFTY) < -16383) input_keys |= KEY_UP;
	    if (SDL_JoystickGetButton(controllerinput, SDL_CONTROLLER_BUTTON_DPAD_DOWN) || SDL_JoystickGetAxis(controllerinput, SDL_CONTROLLER_AXIS_LEFTY) > 16383) input_keys |= KEY_DOWN;
	    if (SDL_JoystickGetButton(controllerinput, SDL_CONTROLLER_BUTTON_A)) input_keys |= KEY_GESTURE;
	    if (SDL_JoystickGetButton(controllerinput, SDL_CONTROLLER_BUTTON_START)) input_keys |= KEY_MENU;
		#if WTH_SDL_DEMOSUPPORT == 1
		};
		#endif
	    
	    if (SDL_JoystickGetAxis(controllerinput, SDL_CONTROLLER_AXIS_RIGHTX) < -16383) xtrakeys |= KEY_LEFT;
	    if (SDL_JoystickGetAxis(controllerinput, SDL_CONTROLLER_AXIS_RIGHTX) > 16383) xtrakeys |= KEY_RIGHT;
	    if (SDL_JoystickGetAxis(controllerinput, SDL_CONTROLLER_AXIS_RIGHTY) < -16383) xtrakeys |= KEY_UP;
	    if (SDL_JoystickGetAxis(controllerinput, SDL_CONTROLLER_AXIS_RIGHTY) > 16383) xtrakeys |= KEY_DOWN;
	    if (SDL_JoystickGetButton(controllerinput, SDL_CONTROLLER_BUTTON_X)) xtrakeys |= KEY_ATTACK;
	}
	
	#ifdef WTHCOMPILER_DEBUG
	editkeys = 0;
	int mx, my;
	int win_width, win_height;
	SDL_GetMouseState(&mx, &my);
	SDL_GetWindowSize(win, &win_width, &win_height);
	if (keystates[SDL_SCANCODE_1]) addObject(
		(mx - cam.x) / (win_width / WIDTH),
		(my - cam.y) / (win_height / HEIGHT),
		INTERTYPE_COAT
	);
	
	if (keystates[SDL_SCANCODE_2]) editkeys = 1;
	
	if (editkeys != 0 && editkeys_prev == 0) {
		level++;
		LoadInRoom();
	};
	editkeys_prev = editkeys;
	editkeys = 0;
	
	if (keystates[SDL_SCANCODE_BACKSPACE]) {
		puts("== DEBUG PRINT START ==");
		printf("\n");
		
		printf("Game state: %i\n", GAME_STATE);
		printf("Current room: %i\n", level);
		puts("Current options:");
		printf("-- Fade ");
		puts( (options[0] & WTHOPTS_NOFADE) ? str_off : str_on);
		printf("-- Devcam ");
		puts( (options[0] & WTHOPTS_DEVCAM) ? str_on : str_off);
		printf("-- Music ");
		puts( (options[0] & WTHOPTS_DOSONG) ? str_on : str_off);
		printf("-- Double Res ");
		puts( (options[0] & WTHOPTS_2XSIZE) ? str_on : str_off);
		printf("-- Fullscreen ");
		puts( (options[0] & WTHOPTS_FULLSC) ? str_on : str_off);
		printf("\n");
		
		printf("Player position: (%i, %i)\n", plr.x, plr.y);
		if (plr.artifacts) {
			puts("Player has artifacts: ");
			if (plr.artifacts & ARTIFACT_BADGE) puts("-- BADGE");
			if (plr.artifacts & ARTIFACT_MIRROR) puts("-- MIRROR");
			if (plr.artifacts & ARTIFACT_KNIFE) puts("-- KNIFE");
			if (plr.artifacts & ARTIFACT_DONUT) puts("-- DONUT");
		} else puts("Player has no artifacts");
		printf("\n");
		
		printf("Interactibles: %i\n", interacts_count);
		puts("== DEBUG PRINT END ==");
	};
	#endif
};

int main(int argc, char *argv[]) {
	if (SDL_Init(SDL_INIT_AUDIO | SDL_INIT_VIDEO | SDL_INIT_EVENTS |
 MIX_INIT_MID | SDL_INIT_JOYSTICK) != 0) return 1;

	#define REALWINTITLE WINTITLE " " VERSION_NUMBER
	win = SDL_CreateWindow(REALWINTITLE, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WIDTH, HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
	render = SDL_CreateRenderer(win, -1, SDL_RENDERER_PRESENTVSYNC);
	#undef REALWINTITLE
	
	SDL_RenderSetLogicalSize(render, WIDTH, HEIGHT); // keep that cwispy 270P 16:9 wesowutiown UwU (now obsolete)
	
	SDL_SetRenderDrawBlendMode(render, SDL_BLENDMODE_NONE);

	#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__) || defined(__APPLE__)
	puts("WARNING: You are running Wake to Hell under a malware OS. Please consider switching to a less harmful system.");
	FILE *malwareOSWarning;
	fopen(malwareOSWarning, "WARNING.txt");
	fprintf(malwareOSWarning, "You are running Wake to Hell under a malware OS. Please consider switching to a less harmful system.");
	fclose(malwareOSWarning);
	#endif
	
	//winsurf = SDL_GetWindowSurface(win);
	
	if ((SDL_NumJoysticks() > 0)) controllerinput = SDL_JoystickOpen(0);

	#ifndef WTHOPTS_SDL_NOMIXER
	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 1, 2048) != 0)
		puts("The game couldn't find a usable audio system. Check your audio devices!");

	music[0] = Mix_LoadMUS("NMARE.mid"); // note: Fluidsynth MIDIs take up lots of memory
	music[1] = Mix_LoadMUS("CRIMSON.it");
	music[2] = Mix_LoadMUS("SHOPKEEP.mod");
	
	sfx[0] = Mix_LoadWAV("sfx/step.wav");
	sfx[1] = Mix_LoadWAV("sfx/jump.wav");
	sfx[2] = Mix_LoadWAV("sfx/slam.wav");
	sfx[3] = Mix_LoadWAV("sfx/coatpickup.wav");
	sfx[4] = Mix_LoadWAV("sfx/dooropen.wav");
	sfx[5] = Mix_LoadWAV("sfx/doorclose.wav");
	sfx[6] = Mix_LoadWAV("sfx/artifact_badge.wav");
	sfx[7] = Mix_LoadWAV("sfx/artifact_mirror.wav");
	sfx[8] = Mix_LoadWAV("sfx/artifact_donut.wav");
	sfx[9] = Mix_LoadWAV("sfx/artifact_knife.wav");
	sfx[10] = Mix_LoadWAV("sfx/paperopen.wav");
	sfx[11] = Mix_LoadWAV("sfx/paperclose.wav");
	#endif

	SDL_SetRenderDrawColor(render, 0, 0x01, 0x90, 255);
	keystates = SDL_GetKeyboardState(NULL); // this was called every time keys() is ran, but apparently it's only needed to be called ONCE :)
	
	plrsprites[0] = IMG_LoadTexture(render, "sprites/plr_idle.png");                // COATED
	plrsprites[1] = IMG_LoadTexture(render, "sprites/plr_walk.png");
	plrsprites[2] = IMG_LoadTexture(render, "sprites/plr_enterdoor.png");
	plrsprites[3] = IMG_LoadTexture(render, "sprites/plr_asleep.png");
	plrsprites[4] = IMG_LoadTexture(render, "sprites/plr_idle1.png");
	plrsprites[5] = IMG_LoadTexture(render, "sprites/plr_idle_coatless.png");       // COATLESS
	plrsprites[6] = IMG_LoadTexture(render, "sprites/plr_walk_coatless.png");
	plrsprites[7] = IMG_LoadTexture(render, "sprites/plr_enterdoor_coatless.png");
	plrsprites[8] = IMG_LoadTexture(render, "sprites/plr_asleep.png");
	plrsprites[9] = IMG_LoadTexture(render, "sprites/plr_idle1.png");

	sprites[WTH_SPR_MENU] = IMG_LoadTexture(render, "sprites/MENU.png");
	sprites[WTH_SPR_FONTMAP] = IMG_LoadTexture(render, "sprites/fontmap.png");
	sprites[WTH_SPR_MENUBUTTONS] = IMG_LoadTexture(render, "sprites/MENUBUTTONS.png");
	sprites[WTH_SPR_UPARROW] = IMG_LoadTexture(render, "sprites/uparrow.png");
	sprites[WTH_SPR_OPTMENU] = IMG_LoadTexture(render, "sprites/OPTIONSMENU.png");
	sprites[WTH_SPR_BG_STREET] = IMG_LoadTexture(render, "sprites/bg.png");
	sprites[WTH_SPR_BG_HOUSE] = IMG_LoadTexture(render, "sprites/bg_house.png");
	sprites[WTH_SPR_BG_OUTDOORS] = IMG_LoadTexture(render, "sprites/bg_outdoors.png");
	sprites[WTH_SPR_BG_HOSPITAL] = IMG_LoadTexture(render, "sprites/bg_hospital.png");
	sprites[WTH_SPR_COATRACK] = IMG_LoadTexture(render, "sprites/coatrack.png");
	sprites[WTH_SPR_DOOR] = IMG_LoadTexture(render, "sprites/door.png");
	sprites[WTH_SPR_ARTIFACTS] = IMG_LoadTexture(render, "sprites/artifacts.png");
	sprites[WTH_SPR_YOU] = IMG_LoadTexture(render, "sprites/you_idle.png");
	sprites[WTH_SPR_DECOR_BED] = IMG_LoadTexture(render, "sprites/bed.png");
	sprites[WTH_SPR_DECOR_MAILBOX] = IMG_LoadTexture(render, "sprites/decor_mailbox.png");
	sprites[WTH_SPR_DECOR_HATRACK] = IMG_LoadTexture(render, "sprites/decor_hatrack.png");
	sprites[WTH_SPR_BOOK_HAMIE] = IMG_LoadTexture(render, "sprites/book.png");
	sprites[WTH_SPR_READ_HAMIE] = IMG_LoadTexture(render, "sprites/read_hamie.png");
	
	running = 1;
	SDL_Event event;
	start();
	signalDraw(DRAW_CHECK2XSIZE);
	signalDraw(DRAW_CHECKFULLSC);
	while (running) {
		while (SDL_PollEvent(&event)) {
			switch (event.type)
			{
				case SDL_QUIT: running = 0; break;
			}
		}
		keys();
        #if WTH_SDL_DEMOSUPPORT == 1
        if (keystates[SDL_SCANCODE_F1] && demoStatus != 2) {
            if (demoStatus != 1) {
                SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_INFORMATION, str_wintitle, "Recording demo...", NULL);
                DaDemo = fopen("WTH_Demo.wthdem", "w");
                demoStatus = 1;
            } else {
                SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_INFORMATION, str_wintitle, "Demo recording stopped", NULL);
                fclose(DaDemo);
                demoStatus = 0;
            };
        }
        if (keystates[SDL_SCANCODE_F2] && demoStatus != 1) {
            if (demoStatus != 2) {
                DaDemo = fopen("WTH_Demo.wthdem", "r");
                if (DaDemo) {
                	SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_INFORMATION, str_wintitle, "Playing back demo...", NULL);
                	demoStatus = 2;
                };
            } else if (demoStatus == 2) {
                SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_INFORMATION, str_wintitle, "Demo playback stopped", NULL);
                fclose(DaDemo);
                demoStatus = 0;
            };
        }
        switch (demoStatus)
        {
            case 1:
                demowrite[0] = input_keys;
                fwrite(demowrite, 1, 1, DaDemo);
                break;
            case 2:
                fread(demowrite, 1, 1, DaDemo);
                input_keys = demowrite[0];
               // printf("%i", demowrite[0]);
                fseek(DaDemo, demoPointer, SEEK_SET);
                demoPointer++;
                break;
        }
        #endif
		#if (HEIGHT != 270)
		cam.y = -CAM_VERTOFFSET;
		#endif
		draw();
		step();
		
		//wintext = SDL_CreateTextureFromSurface(render, winsurf);
		//SDL_RenderCopy(render, wintext, NULL, NULL);
		//SDL_UpdateWindowSurface(win);
		SDL_RenderPresent(render);
		//SDL_DestroyTexture(wintext);
		SDL_RenderClear(render);
		SDL_Delay(1.0f / GAME_FPS);
	}

	printf("Exiting game...\n");
	
	#ifndef WTHOPTS_SDL_NOMIXER
	for (int i = 0; i < 1; i++) {
		Mix_FreeMusic(music[i]);
	}
	#endif
	
	for (int i = 0; i < 10; i++) {
		Mix_FreeChunk(sfx[i]);
	}
	
	SDL_DestroyRenderer(render);
	SDL_DestroyWindow(win);
	SDL_Quit();
	
	return 0;
};
