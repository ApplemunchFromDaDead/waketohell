== WAKE TO HELL ==
A game made with love and hatred by blitzdoughnuts

This is an adventure-esque game where you play as Lukifer Dredd, a former surgeon at an old hospital. Today he's planning to head back and face what he hated to face most... although there are no faces other than his.

Go around and explore the streets of this unknown town in Florida. Find hints, gizmos, and other neat interactions among the many locations you'll cross on the way.

CONTROLS:
[Z] - Gesture / Confirm / Exit book
[Left arrow] and [Right arrow] - Move horizontally
[Up arrow] and [Down arrow] - Interact
[WASD] - Move camera (NEEDS FREECAM ENABLED)

See MANUAL.md for some gameplay tips and such.

This is *FREE SOFTWARE*, all licensed under the Creative Commons 0 with an additional waiver of any remaining rights, to be given to the *public domain*. You should have a copy of the Creative Commons 0 document packaged with this game. You may find the source code to the game in one of the following locations:
	- https://git.coom.tech/blitzdoughnuts/WakeToHell
	- https://gitlab.com/ApplemunchFromDaDead/waketohell
	- https://git.disroot.org/blitzdoughnuts/WakeToHell

The waiver of rights is as follows:

Each contributor to this work agrees that they waive any exclusive rights, including but not limited to copyright, patents, trademark, trade dress, industrial design, plant varieties and trade secrets, to any and all ideas, concepts, processes, discoveries, improvements and inventions conceived, discovered, made, designed, researched or developed by the contributor either solely or jointly with others, which relate to this work or result from this work. Should any waiver of such right be judged legally invalid or ineffective under applicable law, the contributor hereby grants to each affected person a royalty-free, non transferable, non sublicensable, non exclusive, irrevocable and unconditional license to this right.
