/*
GAME.H
This file defines SPECIFICALLY the game logic, completely separate from any libraries.
Drawing and handling of sprites happens in the main_[platform].c file.

Licensed under CC0, public domain, 2023-2024
Made by blitzdoughnuts
*/

#include "constants.h"
#include "config.h"

#define ACHIEVE_BEATME 1
#define ACHIEVE_NOCOAT 1 << 1
#define ACHIEVE_RISQUE 1 << 2
#define ACHIEVE_MYBLUD 1 << 3
#define ACHIEVE_NOTIME 1 << 4

// draw calls
#define DRAW_PLRUPARROW 1

#define DRAW_FADEIN 16 // begin fading from black
#define DRAW_FADEOUT 17 // begin fading into black
#define DRAW_HALTFADE 18 // clear the screen of fading entirely

#define DRAW_CHECK2XSIZE 64
#define DRAW_CHECKFULLSC 65

#define DRAW_UPDATEMENU 127 // for reducing rendering load

// misc calls
#define MISC_PAUSEMUSIC 0
#define MISC_RESUMEMUSIC 1
#define MISC_STOPMUSIC 2
#define MISC_TOGGLEFF 3 // DEPRECATED
#define MISC_EXITGAME 127

#ifndef CAM_BOUNDS
#define CAM_BOUNDS 160 // border size of camera from both directions
#endif

#define INTER_LIMIT 32 // how many interactibles can be on a level at a time?

#define WINTITLE "Wake to Hell"
#define VERSION_NUMBER "0.2.0a"

#define WTH_keyPressed(keyvalue) (input_keys & keyvalue) && !(input_prevkeys & keyvalue)
// easy way to check if a key was down but NOT held, saves space for comment rambling

void signalDraw(uint8_t signal); // interface with main for drawing specific things at specific times
static inline void signalPlaySFX(uint8_t signal); // play sound
static inline void signalPlayMUS(uint8_t signal); // play music
void signalMisc(uint8_t signal); // misc signal

uint8_t GAME_STATE = 0;
uint8_t GAME_FLAGS = 0; // for routes and stuff

typedef struct {
	int x, y, hsp, vsp;
	int hsp_sub, vsp_sub;
	uint8_t flags, artifacts;
	uint16_t idleanimtimer;
	uint8_t state;
	uint8_t animframe, animtimer, animflimit, animindex;
} WTH_Player;

struct {
	uint8_t menuselect;
	uint8_t menuindex:4,
		menulimit:4;
} menu;
// menuindex and menulimit are 4-bit variables (up to 15)

uint8_t bookpage; // upper 4 bits is book type, lower 4 is page number

struct {
	int x, y;
} cam;

typedef struct {
	int x, y;
	uint16_t vars[8];
	uint8_t objID, flags;
} WTH_Interactible;

WTH_Player plr; // the one and only... uh... unstable furry man.
WTH_Interactible interacts[INTER_LIMIT];
uint8_t interacts_count = 0; // convenient counter for all present interactibles

uint8_t level; // all rooms, 256 is overkill but overkill is what's needed
uint8_t fade, fademode; // fade variables, basically alpha
uint8_t fadespeed;

uint8_t options[3];

uint8_t input_keys;
uint8_t input_prevkeys;
uint8_t xtrakeys;

void saveGame();
void loadGame();
// frontend, please add details

uint8_t addObject(int nx, int ny, uint8_t objType) {
	#ifdef WTHCOMPILER_DEBUG
	if (interacts_count >= INTER_LIMIT) {
		printf("addObject: Tried adding object over the limit!");
		return 0;
	};
	#endif
	uint8_t index = interacts_count;
	if (objType == INTERTYPE_ARTIFACT && (plr.artifacts & (1 << interacts[index].vars[0]))) return 0; // is it an artifact the player already has? don't bother existing
	for (uint8_t i = 0; i < 7; i++) {
		interacts[index].vars[i] = 0;
	};
	interacts[index].x = nx;
	interacts[index].y = ny;
	interacts[index].objID = objType;
	interacts[index].flags ^= INTER_ACTIVE;
	interacts_count++;
	
	switch (objType)
	{
		case INTERTYPE_DOOR:
			if (plr.x >= interacts[index].x - 40 && plr.x < interacts[index].x + 40) interacts[index].flags ^= 1 << 2;
			break;
		case INTERTYPE_YOU:
			interacts[index].vars[2] = WTH_PLR_IDLE_LEN;
			break;
	}
	
	/// printf("\nCreated object number %i at (%i, %i) of object type %i", index, interacts[index].x, interacts[index].y, interacts[index].objID);
	return index;
};

static inline void LoadInRoom() {
	for (uint8_t i = 0; i < interacts_count; i++) {
		if ((interacts[i].flags & INTER_ACTIVE)) {
			interacts[i].flags = 0;
		};
		for (uint8_t v = 0; v < 7; v++) interacts[i].vars[v] = 0;
	}
	interacts_count = 0;
	switch (level)
	{
		case ROOM_TEST: // TEST
		    	addObject(164, GROUNDLEVEL, INTERTYPE_DOOR);
			interacts[0].vars[0] = ROOM_HOUSE;
			addObject(320, GROUNDLEVEL, INTERTYPE_YOU);
			addObject(300, 100, INTERTYPE_ARTIFACT);
			addObject(400, 100, INTERTYPE_ARTIFACT);
			addObject(500, 100, INTERTYPE_ARTIFACT);
			addObject(600, 100, INTERTYPE_ARTIFACT);
			addObject(700, 100, INTERTYPE_BOOK);
			addObject(800, 100, INTERTYPE_STAIRS);
			interacts[3].vars[0] = 1;
			interacts[4].vars[0] = 2;
			interacts[5].vars[0] = 3;
			break;
		case ROOM_HOUSE: // house
			addObject(650, 0, INTERTYPE_MOVEBLOCK);
			interacts[0].flags ^= 1;
			addObject(246,200,INTERTYPE_COAT);
			addObject(-48,218,INTERTYPE_DECOR);
			interacts[2].vars[0] = WTH_SPR_DECOR_BED;
			addObject(0, 0, INTERTYPE_MOVEBLOCK);
			addObject(615, GROUNDLEVEL, INTERTYPE_DOOR);
			interacts[4].vars[0] = ROOM_OUTDOORS;
			break;
		case ROOM_OUTDOORS: // outside of house
			addObject(-76, GROUNDLEVEL, INTERTYPE_DOOR);
			interacts[0].vars[0] = ROOM_HOUSE;
			interacts[0].vars[2] = 5;
			addObject(1920, 0, INTERTYPE_TRIGGER);
			interacts[1].vars[0] = ROOM_STREETS;
			addObject(64,GROUNDLEVEL - 100,INTERTYPE_DECOR);
			interacts[2].vars[0] = WTH_SPR_DECOR_MAILBOX;
			addObject(-100, 0, INTERTYPE_MOVEBLOCK);
			break;
		case ROOM_STREETS: // endless streets
			addObject(plr.x, 0, INTERTYPE_MOVETRACK);
			interacts[0].vars[1] = ROOM_H_ENT;
			addObject(1200, GROUNDLEVEL, INTERTYPE_DOOR);
			interacts[1].vars[0] = 61;
			break;
		case ROOM_H_ENT: // hospital door
			interacts[ addObject(270, GROUNDLEVEL, INTERTYPE_DOOR) ].vars[0] = ROOM_HOSPITAL;
			break;
		case ROOM_HOSPITAL: // hospital main lobby (badge artifact)
			if (plr.artifacts >= 15) interacts[ addObject(-20, GROUNDLEVEL, INTERTYPE_DOOR) ].vars[0] = 10;
			break;
		case ROOM_H_PATIENTS:
			addObject(0, 0, INTERTYPE_MOVEBLOCK);
			interacts[ addObject(178, GROUNDLEVEL, INTERTYPE_DOOR) ].vars[0] = 9;
			interacts[ addObject(274, GROUNDLEVEL, INTERTYPE_DOOR) ].vars[0] = 9;
			break;
		case ROOM_BAR: // hamie's bar
			addObject(50, GROUNDLEVEL, INTERTYPE_DOOR);
			interacts[0].vars[0] = ROOM_STREETS;
			interacts[0].vars[2] = 2;
			for (uint8_t i = 1; i < 5; i++) {
				addObject(32 + (32 * i),GROUNDLEVEL,INTERTYPE_DECOR);
				interacts[i].vars[0] = WTH_SPR_DECOR_BARSTOOL;
			};
			break;
		/*
		case 7: // hos. hallway to/from patient rooms
		case 8: // hos. patient rooms (knife artifact)
		case 9: // hos. dark room (doughnut artifact)
		case 10: // hos. Richie room (mirror artiface)
		case 11: // blank void
			break;*/
	}
};

static inline void changeRoom(uint8_t input, uint8_t startAtInter) {
	level = input;
	plr.x = cam.x = 50;
	plr.y = GROUNDLEVEL;
    	cam.y = 0;
	plr.hsp = plr.vsp = plr.hsp_sub = plr.vsp_sub = 0;
	if (plr.flags & FLAG_CANTMOVE) plr.flags ^= FLAG_CANTMOVE;
	if (startAtInter > 0) {
		plr.x = interacts[startAtInter - 1].x;
	};
	LoadInRoom();
};

void interact_step(WTH_Interactible *REF) {
	if (!(REF->flags & INTER_ACTIVE)) return;
	switch (REF->objID) {
		case INTERTYPE_TRIGGER:
			if ((REF->flags & FLAG_HASCOAT) && !(plr.flags & FLAG_HASCOAT)) break; // coat-only trigger
			if (plr.x >= REF->x) {
				changeRoom(REF->vars[0], 0);
			};
			break;
		case INTERTYPE_DOOR:
			/* flag 1 is the entering door state,
			   flag 2 is if the door is open
			   (flag 2 is only really used if
			   transitioning rooms) */
			if (!(REF->flags & 1 << 1)) {
				if (!(plr.flags & FLAG_GROUNDED)) return;
				if (plr.y == REF->y && plr.x >= REF->x - 40 && plr.x < REF->x + 40) {
					// printf("Player can enter door");
					signalDraw(DRAW_PLRUPARROW);
					if ((input_keys & KEY_UP)) {
						signalPlaySFX(4);
						plr.flags ^= FLAG_CANTMOVE | FLAG_HALTANIM;
						plr.animindex = WTH_PLR_DOOR_SPR;
						plr.animframe = plr.animtimer = 0;
						plr.animflimit = WTH_PLR_DOOR_LEN;
						plr.hsp = plr.vsp = 0;
						REF->vars[1] = 40;
						REF->flags ^= 1 << 1;
						signalDraw(DRAW_FADEOUT);
					};
					if ((REF->flags & 1 << 2) && plr.hsp) {
						signalPlaySFX(5);
						REF->flags ^= 1 << 2;
					};
				}
			} else {
				if (REF->vars[1] == 0) {
					plr.flags ^= FLAG_HALTANIM | FLAG_REVERSEANIM;
					plr.animindex = WTH_PLR_DOOR_SPR;
					plr.animflimit = plr.animframe = WTH_PLR_DOOR_LEN;
					changeRoom(REF->vars[0], REF->vars[2]);
					signalDraw(DRAW_FADEIN);
					fadespeed = 4;
				} else {
					if (plr.x - 1 < REF->x) plr.x++;
					if (plr.x + 1 > REF->x) plr.x--;
					REF->vars[1]--;
					fadespeed++;
				};
			};
			break;
		case INTERTYPE_BOOK:
			if (!(REF->flags & 1 << 4)) {
				if (!(plr.flags & FLAG_GROUNDED)) return;
				if (plr.x >= REF->x - 40 && plr.x < REF->x + 40) {
					signalDraw(DRAW_PLRUPARROW);
					if ((input_keys & KEY_UP)) {
						signalPlaySFX(10);
						plr.flags ^= FLAG_CANTMOVE | FLAG_HALTANIM;
						plr.animindex = 2;
						plr.animframe = plr.animtimer = 0;
						plr.animflimit = WTH_PLR_DOOR_LEN;
						plr.hsp = plr.vsp = 0;
						REF->vars[1] = 40;
						REF->flags ^= 1 << 4;
						signalDraw(DRAW_FADEOUT);
					};
				}
			} else {
				if (REF->vars[1] == 0) {
					plr.flags ^= FLAG_HALTANIM;
					plr.animindex = 0;
					plr.animflimit = WTH_PLR_IDLE_LEN;
					GAME_STATE = GSTATE_BOOK;
					signalDraw(DRAW_FADEIN);
					fadespeed = 3;
					REF->vars[1] = 255;
					REF->flags ^= 1 << 4;
				} else {
					if (plr.x - 1 < REF->x) plr.x++;
					if (plr.x + 1 > REF->x) plr.x--;
					REF->vars[1]--;
					fadespeed++;
				};
			};
			break;
		case INTERTYPE_STAIRS:
			/*
			  FLAG 2 = player going up stairs?
			  FLAG 4 = go down?
			  VAR 0 = snapshot of player Y,
			  modified for going up or down
			  VAR 1 = time to wait before
			  doing something
			*/
			if (REF->flags & 2) {
				if (plr.y == REF->vars[0]) {
					REF->flags ^= 2 | 4;
					plr.flags ^= FLAG_CANTMOVE | FLAG_HALTANIM | FLAG_REVERSEANIM;
					break;
				};
				if (REF->flags & 4) plr.y++;
				else plr.y--;
			} else {
				if (plr.y >= REF->y - 64 && plr.x >= REF->x - 40 && plr.x < REF->x + 40) {
					signalDraw(DRAW_PLRUPARROW);
					if (WTH_keyPressed(KEY_UP)) {
						REF->flags ^= 2;
						plr.flags ^= FLAG_CANTMOVE | FLAG_HALTANIM;
						plr.animframe = plr.animtimer = 0;
						if (REF->flags & 4) {
							plr.animindex = WTH_PLR_IDLE1_SPR;
							plr.animflimit = WTH_PLR_IDLE1_LEN;
							REF->vars[0] = plr.y + 64;
						} else {
							plr.animindex = WTH_PLR_DOOR_SPR;
							plr.animflimit = WTH_PLR_DOOR_LEN;
							REF->vars[0] = plr.y - 64;
						};
					};
				};
			};
			break;
		case INTERTYPE_COAT:
			if (plr.x >= REF->x - 40 && plr.x < REF->x + 40) {
				signalDraw(DRAW_PLRUPARROW);
				if (WTH_keyPressed(KEY_UP)) {
					plr.flags ^= FLAG_HASCOAT;
					signalPlaySFX(3);
				};
			};
			break;
		case INTERTYPE_ARTIFACT:
			if (plr.x >= REF->x - 30 && plr.x < REF->x + 30) {
				signalDraw(DRAW_PLRUPARROW);
				if (WTH_keyPressed(KEY_UP)) {
					REF->flags ^= INTER_ACTIVE; // disable it
					plr.artifacts ^= 1 << REF->vars[0];
					signalPlaySFX(6 + REF->vars[0]);
				};
			};
			break;
		case INTERTYPE_MOVETRACK:
			/* imagine that the X of the INSTANCE is like a last-frame
			   X of the player, while the current plr.x is the current
			   frame X; we compare those and change the distance variable accordingly */
			if (REF->x != plr.x) {
				REF->vars[0] += (REF->x < plr.x) ? plr.x - REF->x : REF->x - plr.x;
			};
			if (REF->vars[0] > 2500) {
                		level = REF->vars[1];
				LoadInRoom();
				while (plr.x > 0) {
					plr.x -= 600;
					cam.x -= 600;
				}
			};
			REF->x = plr.x;
			break;
		case INTERTYPE_YOU:
			// 0 = animtimer, 1 = animframe, 2 = animflimit
			// printf("FAKE PLAYER");
			REF->vars[0]++;
			if (REF->vars[0] >= ANIM_SPEED) {
				REF->vars[1]++;
				REF->vars[0] = 0;
			};
			if (REF->vars[1] > REF->vars[2]) REF->vars[1] = 0;
			break;
		case INTERTYPE_MOVEBLOCK:
			/* prevents the player from moving either too
			   far to the left, or too far to the right
			   flag 1 blocks moving right; by default, it
			   only blocks moving left */
			if ((plr.x < REF->x && !(REF->flags & 1)) || (plr.x > REF->x && (REF->flags & 1))) {
				plr.x = REF->x;
				plr.hsp = plr.hsp_sub = 0;
			};
		    break;
	}
};

#ifndef WTHCOMPILER_NOOPTIONSMENU
void manageOptions() {
	if (menu.menuselect < 5) {
		options[0] ^= 1 << menu.menuselect;
		if (menu.menuselect == 4) signalDraw(DRAW_CHECKFULLSC);
		if (menu.menuselect == 3) signalDraw(DRAW_CHECK2XSIZE);
	} else {
		saveGame(); menu.menuindex = menu.menuselect = 0; menu.menulimit = 2;
	};
};
#endif

void start() {
	plr.x = 50;
	plr.y = GROUNDLEVEL;
	plr.state = 1;
	//plr.flags = 0;
	cam.x = cam.y = 0;
	GAME_STATE = 0;
	GAME_FLAGS = 0;
	plr.animflimit = WTH_PLR_IDLE_LEN;
	menu.menuselect = 0;
	menu.menulimit = 2;
	loadGame();
};

void step() {
	switch (GAME_STATE)
	{
	case GSTATE_MENU:
		if (WTH_keyPressed(KEY_UP) && menu.menuselect > 0) {
			menu.menuselect--;
			signalPlaySFX(0);
		};
		if (WTH_keyPressed(KEY_DOWN) && menu.menuselect < menu.menulimit) {
			menu.menuselect++;
			signalPlaySFX(0);
		};
		if (WTH_keyPressed(KEY_GESTURE)) {
			signalPlaySFX(2);
			#ifndef WTHCOMPILER_NOOPTIONSMENU
			if (menu.menuindex == 1 || menu.menuindex == MINDEX_XOPTIONS) {
				manageOptions();
			} else {
			#endif
				switch (menu.menuselect)
				{
					case 0:
						GAME_STATE = GSTATE_PLAYING;
						signalPlayMUS(0);
						signalDraw(DRAW_FADEIN);
						fadespeed = 2;
						changeRoom(0, 0);
						break;
			#ifndef WTHCOMPILER_NOOPTIONSMENU
					case 1: menu.menuindex = 1; menu.menuselect = 0; menu.menulimit = 5; break;
			#endif
					case 2: signalMisc(MISC_EXITGAME); break;
				}
			#ifndef WTHCOMPILER_NOOPTIONSMENU
			};
			#endif
		};
		break;
	case GSTATE_PLAYING:
		if (fademode == 1 && fade > 0) {
			if (fade - fadespeed < 0)
				fade = 0;
			else
				fade -= fadespeed;
		};
		if (fademode == 2 && fade < 255) {
			if (fade + fadespeed > 255)
				fade = 255;
			else
				fade += fadespeed;
		};
		switch (plr.state)
		{
			case PSTATE_NORMAL:
				if (!(plr.flags & FLAG_CANTMOVE)) {
					if ((input_keys & KEY_LEFT) && plr.hsp > -3) {
						plr.hsp_sub -= SUBPIXELUNIT_ACCURACY / 8;
						plr.animindex = WTH_PLR_WALK_SPR;
						plr.animflimit = WTH_PLR_WALK_LEN;
						if (plr.flags & FLAG_HALTANIM) plr.flags ^= FLAG_HALTANIM;
						if (plr.flags & FLAG_REVERSEANIM) plr.flags ^= FLAG_REVERSEANIM;
						if (plr.hsp > 0) plr.hsp = 0;
						if (!(plr.flags & FLAG_FLIPPED)) plr.flags ^= FLAG_FLIPPED;
					};
					if ((input_keys & KEY_RIGHT) && plr.hsp < 3) {
						plr.hsp_sub += SUBPIXELUNIT_ACCURACY / 8;
						plr.animindex = WTH_PLR_WALK_SPR;
						plr.animflimit = WTH_PLR_WALK_LEN;
						if (plr.flags & FLAG_HALTANIM) plr.flags ^= FLAG_HALTANIM;
						if (plr.flags & FLAG_REVERSEANIM) plr.flags ^= FLAG_REVERSEANIM;
						if (plr.hsp < 0) plr.hsp = 0;
						if (plr.flags & FLAG_FLIPPED) plr.flags ^= FLAG_FLIPPED;
					};
					if (!(input_keys & KEY_LEFT) && !(input_keys & KEY_RIGHT) && (plr.flags & FLAG_GROUNDED)) { // seems erroneous
						plr.hsp = 0;
						if (!(plr.flags & FLAG_REVERSEANIM)) {
							if (plr.animindex != WTH_PLR_IDLE_SPR && plr.animindex != WTH_PLR_IDLE1_SPR) {
								plr.animindex = WTH_PLR_IDLE_SPR;
								plr.animflimit = WTH_PLR_IDLE_LEN;
								if (plr.flags & FLAG_HALTANIM) plr.flags ^= FLAG_HALTANIM;
							};
							plr.idleanimtimer++;
							if (plr.idleanimtimer >= 400) {
								plr.animindex = WTH_PLR_IDLE1_SPR;
								plr.animflimit = WTH_PLR_IDLE1_LEN;
								if (!(plr.flags & FLAG_HALTANIM)) plr.flags ^= FLAG_HALTANIM;
								plr.animtimer = 0;
								plr.animframe = 0;
								plr.idleanimtimer = 0;
							};
						} else if (plr.animframe == 0) plr.flags ^= FLAG_REVERSEANIM;
					};
				};
				
				if (!(plr.flags & FLAG_NOPHYS)) {
					if (!(plr.flags & FLAG_GROUNDED)) {
						plr.vsp_sub += PRE_GRAVITY;
					}
					if (plr.y > GROUNDLEVEL || plr.y + plr.vsp > GROUNDLEVEL) {
						if (!(plr.flags & FLAG_GROUNDED)) {
							plr.flags |= FLAG_GROUNDED;
						};
						plr.vsp = 0;
						plr.vsp_sub = 0;
						plr.y = GROUNDLEVEL;
					};
					// if the calc messes up at long distances, try changing it to an int
					if (plr.vsp_sub > SUBPIXELUNIT_ACCURACY) {
					    int16_t calc = (plr.vsp_sub / SUBPIXELUNIT_ACCURACY);
						plr.vsp += calc;
						plr.vsp_sub -= SUBPIXELUNIT_ACCURACY * calc;
					};
					if (plr.vsp_sub < 0) {
					    int16_t calc = (plr.vsp_sub / SUBPIXELUNIT_ACCURACY);
						plr.vsp -= calc;
						plr.vsp_sub += SUBPIXELUNIT_ACCURACY * calc;
					};
					
					if (plr.hsp_sub > SUBPIXELUNIT_ACCURACY) {
					    int16_t calc = (plr.hsp_sub / SUBPIXELUNIT_ACCURACY);
						plr.hsp += calc;
						plr.hsp_sub -= SUBPIXELUNIT_ACCURACY * calc;
					};
					if (plr.hsp_sub < 0) {
					    int16_t calc = (plr.hsp_sub / SUBPIXELUNIT_ACCURACY);
						plr.hsp += calc;
						plr.hsp_sub -= SUBPIXELUNIT_ACCURACY * calc; // HOW does this make ANY logical sense to work properly
					};
					plr.x += plr.hsp;
					plr.y += plr.vsp;
				};
				break;
			case PSTATE_SLEEPING: default:
				plr.animindex = WTH_PLR_SLEEP_SPR;
				plr.animflimit = WTH_PLR_SLEEP_LEN;
				if (input_keys & KEY_GESTURE && !(input_prevkeys & KEY_GESTURE)) {
					plr.state = PSTATE_NORMAL;
					plr.animindex = WTH_PLR_WALK_SPR;
					plr.vsp = -3;
					plr.vsp_sub = 0;
					signalPlaySFX(1);
					plr.flags = 0;
				};
				break;
		}
			
		if (!(options[0] & WTHOPTS_DEVCAM)) {
			if (plr.x > cam.x + (WIDTH - CAM_BOUNDS)) {
				cam.x += plr.x - (cam.x + (WIDTH - CAM_BOUNDS));
			};
			if (plr.x < cam.x + CAM_BOUNDS) {
				cam.x -= (cam.x + CAM_BOUNDS) - plr.x;
			};
		} else {
			uint8_t speed = (xtrakeys & KEY_ATTACK) ? 6 : 3;
			if (xtrakeys & KEY_RIGHT) cam.x += speed;
			if (xtrakeys & KEY_LEFT) cam.x -= speed;
			if (xtrakeys & KEY_UP) cam.y -= speed;
			if (xtrakeys & KEY_DOWN) cam.y += speed;
		};

		plr.animtimer++;
		if (plr.animtimer >= ANIM_SPEED && !(plr.flags & FLAG_HALTANIM && (plr.animframe == ((plr.flags & FLAG_REVERSEANIM) ? 0 : plr.animflimit)))) {
			plr.animframe += (plr.flags & FLAG_REVERSEANIM) ? -1 : 1;
			plr.animtimer = 0;
		};
		if (!(plr.flags & FLAG_HALTANIM)) {
			if (plr.animframe > plr.animflimit) plr.animframe = 0;
		}
		if (plr.flags & FLAG_GROUNDED) {
			if (plr.animframe == 1 && plr.animflimit == WTH_PLR_WALK_LEN && !(plr.flags & FLAG_STEPDEBOUNCE)) {
				signalPlaySFX(0);
				plr.flags += FLAG_STEPDEBOUNCE;
			} else if (plr.animframe != 1 && (plr.flags & FLAG_STEPDEBOUNCE)) plr.flags -= FLAG_STEPDEBOUNCE;
		};
		
		if (WTH_keyPressed(KEY_MENU)) {
			GAME_STATE = GSTATE_PAUSED;
			menu.menuselect = 0;
			menu.menulimit = 5;
			signalMisc(MISC_PAUSEMUSIC);
		};
		
		for (uint8_t i = 0; i < INTER_LIMIT; i++) {
			if ((interacts[i].flags & INTER_ACTIVE)) {
				interact_step(&interacts[i]);
			};
		};
		break;
	case GSTATE_PAUSED:
		if (WTH_keyPressed(KEY_UP) && menu.menuselect > 0) {
			menu.menuselect--;
			signalPlaySFX(0);
		};
		if (WTH_keyPressed(KEY_DOWN) && menu.menuselect < menu.menulimit) {
			menu.menuselect++;
			signalPlaySFX(0);
		};
		if (WTH_keyPressed(KEY_GESTURE)) {
			signalPlaySFX(2);
			if (menu.menuindex == 0) {
				switch (menu.menuselect)
				{
					case 0: GAME_STATE = GSTATE_PLAYING; signalMisc(MISC_RESUMEMUSIC);break;
					//case 1: options[0] ^= WTHOPTS_DEVCAM; break;
					case 1: menu.menuindex = 1; menu.menuselect = 0; break;
					case 2: // exiting from pause menu to main menu; clear all gamevariables!
						plr.artifacts = 0;
						plr.animframe = plr.animindex = plr.animtimer = 0;
						plr.hsp = plr.vsp = plr.hsp_sub = plr.vsp_sub = 0;
						level = 0;
						signalMisc(MISC_STOPMUSIC);
						plr.flags = 0; // if this causes issues, move it to start()
						start();
						break;
				}
			} else manageOptions();
		};
		break;
	case GSTATE_BOOK:
		if (fademode == 1 && fade > 0) {
			if (fade - fadespeed < 0)
				fade = 0;
			else
				fade -= fadespeed;
		};
		if (fademode == 2 && fade < 255) {
			if (fade + fadespeed > 255)
				fade = 255;
			else
				fade += fadespeed;
		};
		if (WTH_keyPressed(KEY_RIGHT) && (bookpage & 0x0F) < 16) {
			bookpage++;
		};
		if (WTH_keyPressed(KEY_LEFT) && (bookpage & 0x0F) > 0) {
			bookpage--;
		};
		if (WTH_keyPressed(KEY_GESTURE)) {
			GAME_STATE = GSTATE_PLAYING;
			fademode = 1;
			fadespeed = 3;
			if (plr.flags & FLAG_CANTMOVE) plr.flags ^= FLAG_CANTMOVE;
			signalPlaySFX(11);
		};
		break;
	}
	
	input_prevkeys = input_keys;
	input_keys = 0;
	if (options[0] & WTHOPTS_DEVCAM) xtrakeys = 0;
};
