# Wake to Hell

A walking simulator / interactive story about a cat who seems to be dealing with some strong issues... to say the least.

It's a public domain, free software game that puts you into the shoes of Lukifer Dredd, who must navigate his way through his day while having to put up with certain oddities across his area that seem to impede him. Meet your worries, meet your demons, meet yourself.

**NOTE: This game is still in alpha!** Things seen in alpha builds may be modified or removed later, and not all intended content is included.

Made with love and hatred by blitzdoughnuts, author of [LUDICRITAL](https://ludicrital.neocities.org) and professional lunatic.

## Why This Game is Special (ft. Anarch)

- Completely free (as in freedom)! Code and assets are CC0 (public domain) with an additional wavier of IP rights, see DOCS/LICENSES.txt for extra details.
- Anti-commercial! No microtransactions, advertising, or capitalist intent here, it already gives us enough of a headache.
- Game code is independent from the platforms, allowing for relatively easy porting! Current platforms include:
    - Windows (SDL2, Raylib)
    - GNU/Linux (SDL2, SAF)
- Compilation is clean! Just a C compiler, a handful of libraries, and optionally the build script are needed.
- Uses ONLY integer math, to drop any floating point dependencies and to piss off sane programmers! :D
- Low hardware demands! All (at least most) code is optimal and small, even including frontends.
- Made with free software! Vim (previously Notepad++) for code editing, LibreSprite for drawing, and the GNU and Tiny C compilers for GNU/Linux and Windows respectively.
- Contains psychological horror themes and, in tandem, borderline philosophical questions. *Woohoo...*
- Has cats!
- Kept simple, stupid! File I/O is not a dependency, it is configurable in either the config.h or the save file, it avoids overcomplicated methods as much as it can, and it is platform agnostic. Doesn't matter if you're on a PC or a bowl of oatmeal; just enjoy the damn game.

Much kudos to [Anarch](https://codeberg.org/drummyfish/Anarch/) by drummyfish for this section's inspiration.

## What the Hell is this?

I made this game for a good reason: not only to exercise my storytelling capability and also to have an excuse to make a game entirely in C and (mainly) SDL2, but also to get out a message.

Honestly, it saddens me to see the current world where, on and outside of the Internet, there's a ton of stress and hecticity surrounding everything nowadays. Especially for people with aspergers, autism, or any form of neurodiversity or mental disorder, it's a double edged sword, even if you DON'T account for the shitfest that is the Internet. The workforce, for one, is an underlooked cause of stress. Not only is it hazardous in [the obvious way](http://www.tastyfish.cz/lrs/capitalism.html), but also in the forced stress it puts on its employees with its manipulation tactics. Even our schools aren't safe.

I decided to vent this over to a tangible interactive medium, to hopefully make more people at least aware of the potential struggles that people in these times go through in social and personal terms, and otherwise provide something memorable in the form of a potentially gripping story. As the world is ever changing, probably for the worse, any nudge in the right direction counts.

tl;dr I'm crazy and I'm sad, so I made a game to say that.

## Compiling

For compiling the SDL2 frontend of the game, you may type ``bash make.sh sdl`` on any GNU/Linux or \*nix system or environment. You'll want the following devel libraries:
    - SDL2 (libsdl2-dev)
    - SDL2 Mixer (libsdl2-mixer-dev)
    - SDL2 Image (libsdl2-image-dev)

You can easily install these with sudo apt-get install libsdl2-dev libsdl2-mixer-dev libsdl2-image-dev for Debian-based GNU/Linux distributions.

Note that ``make.sh`` supports compiler flags as a 2nd argument, so you may do, e.g. ``bash make.sh sdl -DWTHCOMPULER_DEBUG`` for Debug Mode. ``-DWTHCOMPILER_NOOPTIONSMENU`` disables the Settings menu for frontends that don't need it, or if you want to sucklessly compile the game, at least to a degree.

**On Windows, you must have the respective .dll files in the same directory as the .exe to run the game!** Keep this in mind when packaging the game, as well.

## Known Bugs, Issues, and Mishaps

As the game is work in progress (0.1.6a as of writing), there's bound to be bugs and simple incompleteness around.

# Bugs
- RayLib and SAF are behind the SDL2 frontend, and have improper compatibility. May cause issues.
- Some functions, e.g. sheeted sprite rendering, may have useless parameters.
    - A good way to try fixing this is to precache the size of the sprites when the game starts, or outright assume sprite sizes.
- The keyboard status function on SDL2 always throws a warning due to the NULL given as a pointer parameter. May cause issues.

# Mishaps
- Lukifer preserves his horizontal momentun while airborne. (not a concern; jumping's gone)
- Music seems to no longer play
- Holding left and right simultaneously causes Lukifer to walk in place. (TODO: check)
- Reversing the IDLE1 animation causes a loop until Lukifer walks
- SDL2 SPECIFIC:
    - The function for drawing text is limited to 255 characters, meaning overflows may occur.
    - Demo playback might not find the EOF of the demo correctly
    - Demos don't account for things like game settings, player position and status, and game vars like the level and the Interactibles within.
    - When exiting the game and restarting without closing it, the music doesn't go back to the beginning (SDL2 seems to not handle MIDI music positions well)

## Ideas for What You Can Do

Considering that Wake to Hell is given to the public domain, it's no surprise that you may be able to find great use out of this, especially in creative ways.

- Play it, edit it, break it
- Record it, (quick) review it
- Improve in-game features
- Add WTH Level Editor support
- Pretty up the game, or simplify it heavily
- Add proper support for dynamic resolutions
- Base your game off of WTH's codebase
- Port to your favorite platform, e.g. Pokitto
- Put Wake to Hell into Anarch (or vice versa)
- Change the game's artstyle to whatever you wish
- Remove or exagerate the dreadful theme of the game
- Add it to a package repository (Debian/Devuan, Arch)
- Make the game 3D, bonus points for VR :)
- Recreate the game on a different engine, like Godot, Torque, etc.
- Make frontends for the game with, e.g. SFML, X11 and PulseAudio, OpenGL, even bare-metal
- Use the game for education or demonstration
- Remove any and all file I/O and piss off the normies Lmao

## Contributing

You don't.

Well not directly, anyways. You may point out bugs you've discovered, but pull requests/code merges **will not** be accepted, as I want this repository to compromise of only things I have made myself in the name of legal clarity.

## Licensing

**TL;DR everything here is CC0, solidified with a wavier of IP rights; do absolutely WHATEVER you want**

Everything made by blitzdoughnuts in this repository, including but not limited to code, graphics, sound effects, story, and text strings are licensed and waived under Creative Commons 0. For additional legal details, see **DOCS/LICENSES.txt**.

Each contributor to this work agrees that they waive any exclusive rights, including but not limited to copyright, patents, trademark, trade dress, industrial design, plant varieties and trade secrets, to any and all ideas, concepts, processes, discoveries, improvements and inventions conceived, discovered, made, designed, researched or developed by the contributor either solely or jointly with others, which relate to this work or result from this work. Should any waiver of such right be judged legally invalid or ineffective under applicable law, the contributor hereby grants to each affected person a royalty-free, non transferable, non sublicensable, non exclusive, irrevocable and unconditional license to this right.
