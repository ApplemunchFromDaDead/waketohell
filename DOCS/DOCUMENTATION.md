I'll try keeping this short. This really focuses on the SDL2 frontend, but should be applicable for all.

step() and start() functions are game functions, platform independent.

interact\_step(object) runs a "step" for the specified interactible. Usually is ran every frame in step().

draw() function in all frontends details the rendering of all the game assets and the likes

keys() function passes on the key presses in the frontend over to the game's input\_keys.

loadGame() and saveGame() use a file named "WTH\_SaveGame.bin" if the frontend allows file IO.

drawSprite(sprite, X, Y) draws a sprite at a certain on-screen position.
drawRepeatingSprite(sprite, X, Y) is the same, except it tries tiling it across the whole screen.
drawSpriteSheeted(sprite, X, Y, frame, width, height, flipped) draws a frame from a spritesheet at the position given, with or without flipping, and uses the width parameter as a means of determining how to cut the spritesheet frames. Only supports horizontal sheets.
drawTextString(text, x, y, color, font) draws a string on-screen.
signalDraw(index) is used by the game for specific interfacing with rendering, e.g. drawing the arrow above Lukifer's head.

signalPlaySFX(index) and signalPlayMUS(index) play a sound or change music to the chosen table index, e.g. sfx[index] or music[index].

## Macros

WTHOPTS\_NOOPTIONSMENU disables the options menu and makes it inaccessible in-game.
WTHOPTS\_SDL\_NOMIXER disables loading SDL2 Mixer, and removes it as a dependency. Also removes audio for SDL2.

## Save file bits

Byte 1:

0x00000001 - Disables fading in/out if enabled
0x00000010 - Toggle the free cam (controllable by WASD)
0x01000000 - Scale window by a 1x or 2x factor on startup/option change
0x10000000 - Toggle fullscreen

Bytes 2 and 3: ( 1 << n ) [ACHIEVEMENTS]

0 - "An Unwelcoming Symphony" - Beat the game once. Doesn't show a pop-up on achievement.
1 - "Expose Your Dredd" - Forget your coat at the hospital.
2 - "Something Caught My Eye" - Linger on the photobook in Hamie's room for a little too long...
3 - "See My Blood Fall" - Get hurt deliberately. Psychotic bastard...
4 - "I've Got Work In The Morning" - Beat the game as fast as you can.
5 - "Street Patrol" - Explore every area of the town and find every thing within.
6 - "Limes In My Grave" - Take damage from or die to everything possible. Y'know, masochism.
