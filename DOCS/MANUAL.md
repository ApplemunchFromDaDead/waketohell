# Wake to Hell Manual

In case you dumb fucks don't know how to play an interactive story game, this is a tutorial on how to play an interactive story game. This specific one being Wake to Hell, of course.

This will assume you are playing the SDL2 version of Wake to Hell, basically the main PC version, and that you have read the ``readme.txt`` that came with the game (or ``readme_package.txt`` if you compiled from source).

## THE BASICS

In Wake to Hell, you, controlling Lukifer Dredd, awake from your bed at your house. Your abilities are as follows:

- Walking left and right along the ground or some elevated surface
- Climbing stairs or ladders up and down
- Entering doors and interacting with vital objects
- Gesturing, either doing a mildly amusing thing or interacting with less important objects.

Pretty limited, right? Well it does encompass what one can do in reality; walk around, enter doors, climb, interact with things, and do actions of either utter beligerance or complete ingenuosity. Besides, this isn't your Anarch or, whatchamacallit, Jumpy Boy Jump Adventure you have on that bootleg console.

**Remember to take it slow and look around.** There's no exact goal in mind to focus on in this game; no dudes to kill, no goal posts to tumble, no paint to splotch onto the nearest wall or wall-resembling object, probably not even a win-or-lose condition. There ARE some things you can take or use that trigger certain events or conditions, but that's for you to discover. One way to imagine this is to ask yourself: *what if I woke up one day, and the sky doesn't look quite right, and everyone's missing, and the world seems so different now?* Examine how the world changed before your eyes, maybe try figuring out clues as to what may have happened, or what things were like beforehand.

While there is no set goal, the game definitely isn't endless. There are a few endings you may receive with certain conditions. Aside from discovering endings, you may also:
- Find little tid-bits and interactions that may be amusing to watch or add on to a thought train
- Put together the "lore" of the world of Wake to Hell, see how things tick
- Attain your oh-so-beloved bragging rights for finding 100% of the content in the game

Note that the gameplay is intended to be much more relaxed than many games I assume you play. It's less about conflict and pressure and more about discovery. Go find some cool stuff.

## THE SPECIFICS

Let's get more intricate; you know how I mentioned things you can take or use? One of those things are **artifacts.**

Artifacts are, at least to Lukifer, the remnants of your past memories that you can use for either uncovering more mysteries or making the world change around you. There are quite a few you can find amidst the areas you walk along in the game. Some covetly hidden, others not so much.. Your order of collection and the ones you collect in particular may affect certain outcomes.

You notice an arrow above Lukifer's head? That means you're able to interact with something you walked over! This may be a door, an artifact, or some form of important object. To interact, press **UP** when the arrow is there. Some other objects are also interactible, but you may have to **Gesture** to actually interact with them. Regular, important objects aren't trying to hide in the background; objects you gesture to interact with are more sly, but still may look a bit "off" next to what's around it.

If you've read LUGICRITAL, you may ~~not~~ know that Lukifer totes a doctor's coat iconically. But in the game, he doesn't have it. What gives? Maybe, dear player, *you forgot to grab it.* You can find the coat back at home on the hanging rack, and oddly enough, in other places where either a hanging rack or a doctor's coat may be at.

## THE OPTIONS MENU

In the main menu and in-game, you'll be able to access your settings. Even if it's at least somewhat clear, here's a clarification of all options, in order:
- Fade: Toggles the fading in/out effect that cues when beginning the game, going through doors, and other events.
- Freecam: Allows you to use WASD on keyboards or the right analogue stick for controllers to move the camera around freely. It goes as fast as Lukifer walks.
- Double Res: Doubles the window size of the game, for frontends that support it. Has no effect for fullscreen or, in the case of dwm, non-floating windows.
- Fullscreen: Switches fullscreen mode for frontends that support it. Also changes if the game is fullscreened when starting it.
- Music: Toggles music, for frontends that support it. May be MIDI, may be tracker, may even be a streamed audio file, it'll likely turn it off, both during gameplay and before.

Default options are as follows:
- Fade is on
- Freecam is off
- Double Res is on
- Fullscreen is on
- Music is on

Note that, should you be playing SAF or have the game compiled in a suckless fashion, the settings menu will not be available. You can press the button, but it won't do anything.

## MODS

To use a mod for Wake to Hell, you must have the source code. Some example mods are found in the MODS folder. Usually there's two ways of applying mods:
    - If the mod only consists of a **.diff** file, do `git apply [path to .diff file]` or `patch < [path to .diff file]` from the root directory of the source code (where game.h is)
    - If the mod has a **.sh** file in it (usually this mod may also come in a folder), run `bash [path to .sh file]` from the root directory of the source code.

Be sure to check the version of the game that it's compatible with! Some mods may be outdated and will require a downgrade or update to work with later versions of the game.
