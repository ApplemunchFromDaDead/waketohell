## If you take plenty of stuff from Anarch, where's the politics?

*Politics has no place in Florida.*

It simply does not belong in neither Wake to Hell nor LUDICRITAL. The most political I ever got in WTH was the fact that it is reactionary software that sends a message while telling a grim story of anxiety and what it could do to someone. I love Anarch just as much as the next Linux-fag, but come on.

## Isn't this a step backwards?

Sort of. Sure, Wake to Hell is nowhere near advanced in technology, but it aims to take full advantage of that by being less tanky on hardware (this can run on a Raspberry Pi 400, no sweat) and easier to download, hence easier to copy.

It's like a story book; paper, pencil, and a writing language are all you need to open a world of possibilities. Then again, WTH is more of a story than a game, so who am I to say this?

## Why CC0 rather than MIT or copyleft?

CC0 is the least possible legal restriction available for most of the world. Besides, not many people (hell, probably even developers and creators of derivative works) give a shit about attribution, so why not just cut it out and let the author decide? Also, even small things like attribution are able to be considered legal burdens and somewhat [selfish](www.tastyfish.cz/lrs/attribution.html) behavior.

This allows for both redistribution without paranoia (like how you can redistribute (GNU/)Linux CDs, even sell them) and a lack of burden on people who put things from this in their own projects. People tend to attribute as habit anyway when they use third-party libre media for their own projects, too.

## Is this propaganda?

I hope not.

## This FAQ gives off megalomaniac vibes.

Hell the fuck yes it does. WTH is far from the perfect piece of software, brought by divine intellect and wrought from the blood of Miloslav Ciz, but it's at least better than running modern games at 2 FPS, or taking up hundreds of thousands of GB in size, or indirectly being fucked over by either scummy practices like $1 million shoes or game design so bad it's too much of a complement to call it "design".

In fact, Wake to Hell is TRIVIAL compared to, say, Anarch. Then again, more focus is on the story and worldbuilding than the gameplay, which warrants more artistic detail than it does technical detail, and while it's no excuse to be trivial, the goal isn't exactly to have a game that's plenty in flash and flair, but hardly has substance (some UE4 games, even commercial ones, manage to fall into this).

It may be a niche story game about a cat, but damnit if it's not a niche story game about a cat with a message.

## How do I compile the game sucklessly?

Currently (16 May, 2024) your best bet is to use SAF, as the executable it produces is entirely self-contained. The SDL2 frontend can be compiled with the -DWTHCOMPILER\_NOOPTIONSMENU flag to disable in-game settings, removing one I/O layer. The frontend itself is still minimalist, but it uses a lot of I/O for loading assets.

## Why are there so many offensive or provocative things in the game and its text files?

As Wake to Hell is a game heavily based on LRS principles, political correctness is basically abolished. This means no coddling to non-offensive things, no matter what the cost for it is. This also means blood, swearing, risque things and stuff. I won't go in-depth here, but Drummyfish has a good writing about it on his LRS Wiki [here](hhtp://www.tastyfish.cz/lrs/political_correctness.html).
