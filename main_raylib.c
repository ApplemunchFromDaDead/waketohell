/*
DEPRECATED Raylib frontend for the game

Licensed under CC0, public domain, 2023-2024
Made by blitzdoughnuts
*/

/*
TO-DO:
* Implement fading in/out
* Implement proper bitmap font drawing (Raylib default can do as a placeholder)
* Update the draw function to match with SDL2
* FIX THE KEYS. I wanna use the arrows ;-;
* Properly implement the signalDraw and signalPlayMUS functions
* Check for redundancies or leftovers from SDL2
*/

#include <stdio.h>

#include <raylib.h>

// try preserving Raylib's keys
#define RAY_LEFT KEY_LEFT
#define RAY_RIGHT KEY_RIGHT
#define RAY_UP KEY_UP
#define RAY_DOWN KEY_DOWN
#undef KEY_LEFT
#undef KEY_RIGHT
#undef KEY_UP
#undef KEY_DOWN

#define BGBLUE (Color){0x01, 0x90, 255}

#include "game.h" // keep in mind, MAIN can still access variables from the game!

static const uint8_t fontOffsets_left[90] = {
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 3, 2, 4, 2, 4, 3, 0, 3, 1, 6, 5, 1, 4, 4, 5, 4, 6, 2, 4, 4, 6, 2, 2, 6, 4,
	6, 6, 9, 10, 3, 9,
	8, 9, 6, 9, 8, 8, 8, 6, 12, 7, 8, 10, 2, 6, 6, 7, 6, 8, 8, 7, 6, 7, 2, 7, 8, 7,
	1, 3, 2, 4, 2, 4, 3, 0, 3, 1, 6, 5, 1, 4, 4, 5, 4
};

static char str_on[] = "ON";
static char str_off[] = "OFF";
static char str_exit[] = "Exit";
static char str_savegamename[] = "WTH_SaveGame.bin";

Music music;

Sound sfx[12];

Texture2D sprites[20];
Texture2D plrsprites[10];

Rectangle dsp_rect; // this is still used for the sprite's boundaries

// drawSprite() is redundant lmao

void drawRepeatingSprite(Texture2D sprite, int x, int y) { // this DOESN'T repeat yet, because I am big Stupid(TM)
	int _x = x;
	dsp_rect.x = dsp_rect.y = 0;
	dsp_rect.width = sprite.width;
	dsp_rect.height = sprite.height;
	uint8_t limit = 0;
	for (; _x < WIDTH && limit <= 30; limit++) {
		if (_x < (-WIDTH << 2)) {
			_x += sprite.width;
			continue;
		};
		DrawTextureRec(sprite, dsp_rect, (Vector2){x,y}, WHITE);
		dsp_rect.x +=dsp_rect.width;
		_x +=dsp_rect.width;
	};
};

void drawSpriteSheeted(Texture2D sprite, int x, int y, int frame, int x_sprdist, int y_sprdist, uint8_t flip) { // supports only purely horizontal sheets
	if (x <= -x_sprdist || x > x_sprdist + WIDTH) return;
	if (y <= -y_sprdist || y > y_sprdist + HEIGHT) return;
	dsp_rect.x = x_sprdist * frame;
	dsp_rect.y = 0;
	dsp_rect.width = x_sprdist;
	dsp_rect.height = y_sprdist;
	if (flip) dsp_rect.width = -dsp_rect.width;
	DrawTextureRec(sprite, dsp_rect, (Vector2){x,y}, WHITE);
};

void drawTextString(const char *stuff, int x, int y, uint8_t color) {
	// to-do: not this shit.
	// I should get to optimizing this
	//switch (color)
	//{
		//case 1: SDL_SetTextureColorMod(sprites[5], 255, 0, 0); break;
		/*
		case 2: SDL_SetTextureColorMod(sprites[5], 0, 255, 0); break;
		case 3: SDL_SetTextureColorMod(sprites[5], 0, 0, 255); break;
		case 4: SDL_SetTextureColorMod(sprites[5], 255, 255, 0); break;
		case 5: SDL_SetTextureColorMod(sprites[5], 0, 255, 255); break;
		case 6: SDL_SetTextureColorMod(sprites[5], 255, 0, 255); break;
		case 7: SDL_SetTextureColorMod(sprites[5], 0, 255, 255); break;
		case 8: SDL_SetTextureColorMod(sprites[5], 255, 255, 0); break;
		case 9: SDL_SetTextureColorMod(sprites[5], 255, 255, 255); break;
		case 10: SDL_SetTextureColorMod(sprites[5], 0, 0, 0); break;*/
		// these extra colors ain't used
		//default: SDL_SetTextureColorMod(sprites[5], 255, 255, 255); break;
	//}
	uint16_t _x = x;
	for (uint8_t i = 0; i < 255; i++) {
		if (stuff[i] == '\0') break; // terminator character? then get outta there before stuff gets nasty
		drawSpriteSheeted(sprites[5], _x, y, stuff[i] - 33, 30, 30, 0);
		_x += (stuff[i] == 32) ? 30 : 30 - fontOffsets_left[stuff[i] - 33];
	};
};

void signalPlaySFX(uint8_t signal) {
	PlaySound(sfx[signal]);
};

void signalMisc(uint8_t signal) {
    switch (signal)
    {
        case MISC_PAUSEMUSIC: case MISC_RESUMEMUSIC: case MISC_STOPMUSIC:
            if (!(options[0] & WTHOPTS_DOSONG)) break;
            switch (signal)
            {
                case MISC_PAUSEMUSIC: PauseMusicStream(music); break;
                case MISC_RESUMEMUSIC: ResumeMusicStream(music); break;
                case MISC_STOPMUSIC: StopMusicStream(music); break;
            }
            break;
    }
};

void drawOptions() {
	drawTextString("Fade", 20,20, menu.menuselect == 0 ? 1 : 0);
	drawTextString((options[0] & WTHOPTS_NOFADE) ? str_off : str_on, 320, 20, menu.menuselect == 0 ? 1 : 0);
	drawTextString("Freecam", 20,52, menu.menuselect == 1 ? 1 : 0);
	drawTextString((options[0] & WTHOPTS_DEVCAM) ? str_on : str_off, 320, 52, menu.menuselect == 1 ? 1 : 0);
	drawTextString("Double Res", 20,84, menu.menuselect == 2 ? 1 : 0);
	drawTextString((options[0] & WTHOPTS_2XSIZE) ? "YES" : "NO", 320, 84, menu.menuselect == 2 ? 1 : 0);
	drawTextString("Fullscreen", 20,116, menu.menuselect == 3 ? 1 : 0);
	drawTextString((options[0] & WTHOPTS_FULLSC) ? str_on : str_off, 320, 116, menu.menuselect == 3 ? 1 : 0);
	drawTextString("Music", 20,148, menu.menuselect == 4 ? 1 : 0);
	drawTextString((options[0] & WTHOPTS_DOSONG) ? str_on : str_off, 320, 148, menu.menuselect == 4 ? 1 : 0);
	
	drawTextString(str_exit, 20,200, menu.menuselect == 5 ? 1 : 0);
};

void signalDraw(uint8_t index) {
	switch (index)
	{
		/*
		case DRAW_FADEIN: case DRAW_FADEOUT: case DRAW_HALTFADE:
			if (options[0] & WTHOPTS_NOFADE) break;
			switch (index)
			{
				case DRAW_FADEIN:
					fade = 255;
					fademode = 1;
					SDL_SetRenderDrawBlendMode(render, SDL_BLENDMODE_BLEND);
					break;
				case DRAW_FADEOUT:
					fade = 0;
					fademode = 2;
					SDL_SetRenderDrawBlendMode(render, SDL_BLENDMODE_BLEND);
					break;
				case DRAW_HALTFADE:
					fade = 0;
					fademode = 0;
					SDL_SetRenderDrawBlendMode(render, SDL_BLENDMODE_NONE);
					break;
			};
			break;
		*/
		case DRAW_PLRUPARROW:
            DrawTexture(sprites[7], plr.x - 20 - cam.x, plr.y - 150 - cam.y, WHITE);
            break;
		case DRAW_CHECK2XSIZE:
			if (options[0] & WTHOPTS_2XSIZE)
				SetWindowSize(960, 540);
			else
				SetWindowSize(480, 270);
			//SDL_SetWindowPosition(win, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);
			break;
		case DRAW_CHECKFULLSC:
			if ((options[0] & WTHOPTS_FULLSC) ? !IsWindowFullscreen() : IsWindowFullscreen()) ToggleFullscreen();
			break;
	};
};

void signalPlayMUS(uint8_t index) {
	switch (index) {
		case 0: default:
			music = LoadMusicStream("NMARE.mid");
			break;
		case 1:
			music = LoadMusicStream("SHOPKEEP.mod");
			break;
	}
	PlayMusicStream(music);
};

void saveGame() {
	FILE *DaSave = fopen("WTH_SaveGame.bin","w");
	fwrite(&options, 1, 1, DaSave);
	fclose(DaSave);
};

void loadGame() {
	FILE *DaSave = fopen("WTH_SaveGame.bin","r");
	fread(&options, 1, 1, DaSave);
	fclose(DaSave);
};

void draw() {
	if (fade >= 255) return;
	switch (GAME_STATE)
	{
		case 0:
			//SDL_FillRect(winsurf, NULL, 400);
			ClearBackground(BGBLUE);
			switch (menu.menuindex)
			{
				case 0:
					DrawTexture(sprites[2], 0, 0, WHITE);
					break;
				case 1:
					drawTextString("Fade", 20,50, menu.menuselect == 0 ? 1 : 0);
					drawTextString((options[0] & WTHOPTS_NOFADE) ? str_off : str_on, 320, 50, menu.menuselect == 0 ? 1 : 0);
					drawTextString("Freecam", 20,82, menu.menuselect == 1 ? 1 : 0);
					drawTextString((options[0] & WTHOPTS_DEVCAM) ? str_on : str_off, 320, 82, menu.menuselect == 1 ? 1 : 0);
					drawTextString("Double Res", 20,114, menu.menuselect == 2 ? 1 : 0);
					drawTextString((options[0] & WTHOPTS_2XSIZE) ? "YES" : "NO", 320, 114, menu.menuselect == 2 ? 1 : 0);
					drawTextString("Fullscreen", 20,146, menu.menuselect == 3 ? 1 : 0);
					drawTextString((options[0] & WTHOPTS_FULLSC) ? str_on : str_off, 320, 146, menu.menuselect == 3 ? 1 : 0);
					
					drawTextString(str_exit, 20,200, menu.menuselect == 4 ? 1 : 0);
					break;
			}
			break;
		case 1:
			ClearBackground(WHITE);

			switch (level)
			{
				case 0:
					drawRepeatingSprite(sprites[0], 0 - cam.x, 0 - cam.y);
					break;
				case 1:
					drawRepeatingSprite(sprites[3], 0 - cam.x, 0 - cam.y);
					break;
				case 2:
					drawRepeatingSprite(sprites[8], 0 - cam.x, 0 - cam.y);
					break;
			};
			
			drawSpriteSheeted(plrsprites[plr.animindex], (plr.x - 75) - cam.x, (plr.y - 100) - cam.y, plr.animframe, 150, 150, (plr.flags & FLAG_FLIPPED) ? 1 : 0);
			
			for (uint8_t i = 0; i < interacts_count; i++) {
				if (!(interacts[i].flags & INTER_ACTIVE)) continue;
				switch (interacts[i].objID)
				{
					case 255: 
						drawSpriteSheeted(sprites[4], (interacts[i].x - 75) - cam.x, (interacts[i].y - 100) - cam.y, interacts[i].vars[1], 150, 150, (plr.x < interacts[i].x) ? 1 : 0);
						break;
					case INTERTYPE_COAT:
						drawSpriteSheeted(sprites[6], interacts[i].x - 35 - cam.x, (interacts[i].y - 100) - cam.y, interacts[i].vars[0] ? 1 : 0, 70, 100, 0);
						break;
				}
			};
			/*
			if (fade != 0) {
				SDL_SetRenderDrawColor(0,0,0, fade);
				SDL_RenderFillRect(NULL);
			};
			*/
			switch (level)
			{
				case 0: drawTextString("Test Room", 0, 0, 0); break;
				case 1: drawTextString("House", 0, 0, 0); break;
				case 2: drawTextString("Outside of House", 0, 0, 0); break;
				default: drawTextString("Unknown Room", 0, 0, 0); break;
			};
			break;
	};
};

void keys() {
	if (IsKeyDown(RAY_LEFT) || IsKeyDown(KEY_A)) {
		input_keys += KEY_LEFT;
	};
	if (IsKeyDown(RAY_RIGHT) || IsKeyDown(KEY_D)) {
		input_keys += KEY_RIGHT;
	};
	if (IsKeyDown(RAY_UP) || IsKeyDown(KEY_W)) {
		input_keys += KEY_UP;
	};
	if (IsKeyDown(RAY_DOWN) || IsKeyDown(KEY_S)) {
		input_keys += KEY_DOWN;
	};
	if (IsKeyDown(KEY_Z)) {
		input_keys += 1 << 4;
	};
	
	if (IsKeyDown(KEY_H)) xtrakeys += KEY_LEFT;
	if (IsKeyDown(KEY_K)) xtrakeys += KEY_RIGHT;
	if (IsKeyDown(KEY_U)) xtrakeys += KEY_UP;
	if (IsKeyDown(KEY_J)) xtrakeys += KEY_DOWN;
};

int main() {
	InitWindow(WIDTH, HEIGHT, WINTITLE);
	InitAudioDevice();
	
	SetTargetFPS(60);

	//music = LoadMusicStream("WTH.mp3");
	
	sfx[0] = LoadSound("sfx/step.wav");
	sfx[1] = LoadSound("sfx/jump.wav");
	sfx[2] = LoadSound("sfx/slam.wav");
	sfx[3] = LoadSound("sfx/coatpickup.wav");
	sfx[4] = LoadSound("sfx/dooropen.wav");
	sfx[5] = LoadSound("sfx/doorclose.wav");
	sfx[6] = LoadSound("sfx/artifact_badge.wav");
	sfx[7] = LoadSound("sfx/artifact_mirror.wav");
	sfx[8] = LoadSound("sfx/artifact_donut.wav");
	sfx[9] = LoadSound("sfx/artifact_knife.wav");
	sfx[10] = LoadSound("sfx/pageopen.wav");
	sfx[11] = LoadSound("sfx/pageclose.wav");
	
	plrsprites[0] = LoadTexture("sprites/plr_idle.png");
	plrsprites[1] = LoadTexture("sprites/plr_walk.png");
	plrsprites[2] = LoadTexture("sprites/plr_enterdoor.png");
	plrsprites[3] = LoadTexture("sprites/plr_asleep.png");
	plrsprites[4] = LoadTexture("sprites/plr_idle_coatless.png");
	plrsprites[5] = LoadTexture("sprites/plr_walk_coatless.png");
	plrsprites[6] = LoadTexture("sprites/plr_enterdoor_coatless.png");
	plrsprites[7] = LoadTexture("sprites/plr_asleep.png");
	plrsprites[8] = LoadTexture("sprites/plr_idle1.png");
	sprites[0] = LoadTexture("sprites/bg.png");
	sprites[1] = LoadTexture("sprites/PLAY.png");
	sprites[2] = LoadTexture("sprites/MENU.png");
	sprites[3] = LoadTexture("sprites/bg_house.png");
	sprites[4] = LoadTexture("sprites/you_idle.png");
	sprites[5] = LoadTexture("sprites/texts/fontmap.png");
	sprites[6] = LoadTexture("sprites/coatrack.png");
	sprites[7] = LoadTexture("sprites/uparrow.png");
	sprites[8] = LoadTexture("sprites/bg_outdoors.png");
	sprites[9] = LoadTexture("sprites/artifacts.png");
	sprites[10] = LoadTexture("sprites/bed.png");
	sprites[11] = LoadTexture("sprites/MENUBUTTONS.png");
	sprites[12] = LoadTexture("sprites/door.png");
	sprites[13] = LoadTexture("sprites/bg_hospital.png");
	sprites[14] = LoadTexture("sprites/texts/fontmap_drummy.png");
	sprites[15] = LoadTexture("sprites/decor_mailbox.png");
	sprites[16] = LoadTexture("sprites/decor_hatrack.png");
	sprites[17] = LoadTexture("sprites/MENU_ff.png");
	sprites[18] = LoadTexture("sprites/artifacts_ff.png");
	sprites[19] = LoadTexture("sprites/book_hamie.png");
	
	start();
	while (!WindowShouldClose()) {
		keys();
		BeginDrawing();
		draw();
		EndDrawing();
		step();
		UpdateMusicStream(music);
	}
	
	//for (int i = 0; i < 1; i++) {
	//	Mix_FreeMusic(music[i]);
	//}
	
	return 0;
};
