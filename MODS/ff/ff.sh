#!/bin/sh

# Family Friendly / "Censor Mode" mod for Wake to Hell
# This used to be a proper game feature, but was taken
# out for less retarded reasons. This mod will recreate
# the effects of Censor Mode.

# get the mod directory from $0, assuming this file is in there
MODDIR=$(dirname $0)

cp $MODDIR/sprites .

cat game.h | sed "s/#define WINTITLE \"Wake to Hell\"/#define WINTITLE \"Wake to Chaos\"/" > game.h
