# !/bin/sh
# Make script for Wake to Hell, includes frontends and packaging
# Licensed under CC0, public domain

COMPILER="gcc"
AUTORUN=1

if [ ! $1 ]; then
	echo "Please give an argument for the target frontend. (sdl, saf, etc)"
	exit 1
fi

if [ $1 = "sdl" ]; then
	SDL_LINK="-lSDL2"
	SDLMIXER_LINK="-lSDL2_mixer"
	SDLIMAGE_LINK="-lSDL2_image"
	# These are here so swapping to static compilation is easier
	
	$COMPILER main_sdl.c -std=c99 $SDL_LINK $SDLMIXER_LINK $SDLIMAGE_LINK -O1 -Wall -Wextra $2 -o WakeToHell
	if [ AUTORUN > 0 ]; then
		./WakeToHell
	fi
elif [ $1 = "saf" ]; then
	$COMPILER main_saf.c -std=c99 -lSDL2 -DWTHCOMPILER_NOOPTIONSMENU $2 -o WakeToHellSAF
elif [ $1 = "package" ]; then
	BUILDDIR="BUILD"
	BUILDEXISTS=0

	if [ ! -d $BUILDDIR ]; then
		mkdir $BUILDDIR
	fi

	cp WakeToHell ${BUILDDIR}
	cp *.mid ${BUILDDIR}
	cp *.mod ${BUILDDIR}
	cp -r sfx ${BUILDDIR}/sfx
	cp -r sprites ${BUILDDIR}
	cp DOCS/LICENSE* ${BUILDDIR}
	cp readme_package.txt ${BUILDDIR}/readme.txt
	cp DOCS/MANUAL.md ${BUILDDIR}
	echo "The game (assuming your binary is named WakeToHell) and all required files for a *NIX build are now in the $BUILDDIR folder."
elif [ $1 = "deb" ]; then
	DEBDIR="PACKAGING/usr/games/waketohell/"
	BUILDDIR="BUILD"

	cp -r $BUILDDIR/* $DEBDIR
elif [ $1 = "love" ]; then # lol
	echo "I can imagine where this is going."
fi
