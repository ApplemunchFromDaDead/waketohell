/*
WAKE TO HELL TOOL

This is a specialized tool that can edit savegames,
perform automatic code generation, and more.

This is used within the command line.
Licensed under CC0, made by blitzdoughnuts / Mr. Wendigo
*/

#include <stdio.h>

#define WTHOPTS_NOFADE 1 // disables fading in/out for performance reasons
#define WTHOPTS_DEVCAM 1 << 1 // a toggle for the free camera
#define WTHOPTS_2XSIZE 1 << 6 // 2x the res?
#define WTHOPTS_FULLSC 1 << 7 // fullscreen enabled?

FILE *data;
uint8_t options[1];

char str_on[] = "ON";
char str_off[] = "OFF";
char str_single[] = "1x";
char str_double[] = "2x";

char userinput;
char userinput2[1];

uint8_t findSubstring(char *input, char *mask, uint8_t masklen, uint8_t offset) {
	for (uint8_t i = offset; i < masklen + offset; i++) {
		if (!input[i + offset] == mask[i]) return 0;
	};
	return 1;
};

int main(int argc, char *argv[]) {
	printf("Wake to Hell Tool by blitzdoughnuts\n");
	printf("To use, open a command line (CMD for Windows, terminal of choice for Linux) and run this:\n");
	printf(argv[0]);
	printf(" [input file]\n\n");
	if (argc > 1) {
		if (findSubstring(argv[1], "-edit", 5, 0)) {
			if (!(data = fopen(argv[2], "rw"))) printf("This file can't be opened, or there was an error in the process of doing so."); return 1;
			fread(&options, 1, 1, data);
			printf("The loaded file has the following detectable data:\n\n");
			if (findSubstring(argv[2], "WTH_SaveData", 12, 0)) { // doing "argv[0] == "WTH_SaveData.bin" doesn't work, so I had to make a FUNCTION just to find if it's save data. jeez
				printf("The file's name suggests that it's save data.\n");
				
				printf("Options byte value is %i", options[0]);
				printf("\n");
			
				printf("Fading is ");
				printf(options[0] & WTHOPTS_NOFADE ? str_off : str_on);
				printf("\n");
				
				printf("Free Camera (devcam) is ");
				printf(options[0] & WTHOPTS_DEVCAM ? str_on : str_off);
				printf("\n");
				
				printf("Window size is ");
				printf(options[0] & WTHOPTS_2XSIZE ? str_double : str_single);
				printf("\n");
				
				printf("Fullscreen is ");
				printf(options[0] & WTHOPTS_FULLSC ? str_on : str_off);
				printf("\n");
				
				printf("What would you like to do?\n\n)1 - Edit file\n)2 - Exit\n");
				scanf(" %c", &userinput);
				switch (userinput)
				{
					case '1': 
						printf("Press keys 1 from 8 to toggle the bits representing these options:\n");
						printf("Bit 1 - Fading\n");
						printf("Bit 2 - Devcam\n");
						printf("Bit 3 - Unused\n");
						printf("Bit 4 - Unused\n");
						printf("Bit 5 - Unused\n");
						printf("Bit 6 - Unused\n");
						printf("Bit 7 - Window Scale\n");
						printf("Bit 8 - Fullscreen\n");
						scanf(" %c", &userinput);
						switch (userinput)
						{
							case '1': options[0] ^= WTHOPTS_NOFADE; break;
							case '2': options[0] ^= WTHOPTS_DEVCAM; break;
							case '3': options[0] ^= 0b00000100; break;
							case '4': options[0] ^= 0b00001000; break;
							case '5': options[0] ^= 0b00010000; break;
							case '6': options[0] ^= 0b00100000; break;
							case '7': options[0] ^= WTHOPTS_2XSIZE; break;
							case '8': options[0] ^= WTHOPTS_FULLSC; break;
							case 48: case 57:
								printf("There's no such thing as 0th or 9th bits in a byte. Try again");
								break;
							default:
								printf("What the hell is a %cth bit!?", userinput);
								break;
						}
						break;
					//case '2': break;
					default:
						if (userinput < 48 || userinput > 57)
							printf("That's not a number. Idiot.");
						break;
				};
			};
			fwrite(options, 1, 1, data);
			fclose(data);
		};
	} else {
		printf("No command line arguments were given!");
	};
}