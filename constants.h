/*
Constants definition file for Wake to Hell

Licensed under CC0, public domain, 2023-2024
Made by blitzdoughnuts
*/

// interactible types
#define INTERTYPE_TRIGGER 0
#define INTERTYPE_DOOR 1
#define INTERTYPE_ARTIFACT 2
#define INTERTYPE_MOVETRACK 3
#define INTERTYPE_COAT 4
#define INTERTYPE_MOVEBLOCK 5
#define INTERTYPE_BOOK 6
#define INTERTYPE_STAIRS 7
#define INTERTYPE_GESTURE 253
#define INTERTYPE_DECOR 254
#define INTERTYPE_YOU 255

// rooms
#define ROOM_TEST 255
#define ROOM_HOUSE 0
#define ROOM_OUTDOORS 1
#define ROOM_STREETS 2
#define ROOM_H_ENT 3
#define ROOM_HOSPITAL 4
#define ROOM_H_PATIENTS 5
#define ROOM_BAR 63

// artifacts
#define ARTIFACT_BADGE 0 // lime badge
#define ARTIFACT_MIRROR 1 // hand mirror 
#define ARTIFACT_DONUT 2// mysterious doughnuts
#define ARTIFACT_KNIFE 3 // blood-stained knife

// input keys
#define KEY_UP 1
#define KEY_DOWN 1 << 1
#define KEY_LEFT 1 << 2
#define KEY_RIGHT 1 << 3
#define KEY_GESTURE 1 << 4
#define KEY_ATTACK 1 << 5
#define KEY_MENU 1 << 6

// menu indexes
#define MINDEX_MAIN 0
#define MINDEX_OPTIONS 1
#define MINDEX_XOPTIONS 2

// ootions flags
#define WTHOPTS_NOFADE 1 // disables fade
#define WTHOPTS_DEVCAM 1 << 1 // a toggle for the free camera
#define WTHOPTS_DOSONG 1 << 2 // toggles music
#define WTHOPTS_2XSIZE 1 << 3 // toggle 2x res when windowed
#define WTHOPTS_FULLSC 1 << 4 // fullscreen enabled?

// flags
#define FLAG_GROUNDED 1                // on ground?
#define FLAG_CANTMOVE 1 << 1           // disables voluntary movement (revokes player control)
#define FLAG_NOPHYS 1 << 2             // disables velocity
#define FLAG_FLIPPED 1 << 3            // left if on, right if off
#define FLAG_STEPDEBOUNCE 1 << 4       // for player, flag to not spam step sounds on a frame
#define FLAG_HASCOAT 1 << 5            // for player: does the player have the coat?
#define FLAG_HALTANIM 1 << 6           // stops animations from looping
#define FLAG_REVERSEANIM 1 << 7        // animations play backwards
#define INTER_ACTIVE 1 << 7            // for interactibles; is this interactible active?

// game flags for e.g. routes
#define FLAG_HAMIEBOOK 1	       // player has read Hamie's photobook?
#define FLAG_LIMEBOOK 1 << 1           // player has read the hospital handbook?

// player states
#define PSTATE_NORMAL 0
#define PSTATE_SLEEPING 1
#define PSTATE_CUTSCENE 2
#define PSTATE_STAIRSUP 3

// game states
#define GSTATE_MENU 0
#define GSTATE_PLAYING 1
#define GSTATE_PAUSED 2
#define GSTATE_BOOK 3

// sprite numbers and lengths
#define WTH_PLR_IDLE_SPR 0
#define WTH_PLR_IDLE_LEN 1
#define WTH_PLR_IDLE1_SPR 4
#define WTH_PLR_IDLE1_LEN 3
#define WTH_PLR_WALK_SPR 1
#define WTH_PLR_WALK_LEN 4
#define WTH_PLR_DOOR_SPR 2
#define WTH_PLR_DOOR_LEN 2
#define WTH_PLR_SLEEP_SPR 3
#define WTH_PLR_SLEEP_LEN 0

#define WTH_SPR_MENU 0
#define WTH_SPR_MENUBUTTONS 1
#define WTH_SPR_FONTMAP 2
#define WTH_SPR_UPARROW 3
#define WTH_SPR_OPTMENU 4
#define WTH_SPR_BG_STREET 5
#define WTH_SPR_BG_HOUSE 6
#define WTH_SPR_BG_OUTDOORS 7
#define WTH_SPR_BG_HOSPITAL 8
#define WTH_SPR_COATRACK 9
#define WTH_SPR_DOOR 10
#define WTH_SPR_ARTIFACTS 11
#define WTH_SPR_YOU 12
#define WTH_SPR_DECOR_MAILBOX 13
#define WTH_SPR_DECOR_BED 14
#define WTH_SPR_DECOR_HATRACK 15
#define WTH_SPR_DECOR_BARSTOOL 16
#define WTH_SPR_DECOR_TARGET 17
#define WTH_SPR_BOOK_HAMIE 18
#define WTH_SPR_READ_HAMIE 19

// sound effect indexes
#define WTH_SFX_STEP 0
#define WTH_SFX_JUMP 1
#define WTH_SFX_SLAM 2
#define WTH_SFX_COAT 3
#define WTH_SFX_DOOR_OPEN 4
#define WTH_SFX_DOOR_CLOSE 5
#define WTH_SFX_ARTIFACT_BADGE 6
#define WTH_SFX_ARTIFACT_MIRROR 7
#define WTH_SFX_ARTIFACT_DONUT 8
#define WTH_SFX_ARTIFACT_KNIFE 9
#define WTH_SFX_PAGE_OPEN 10
#define WTH_SFX_PAGE_CLOSE 11

// unorganized
#define SUBPIXELUNIT_ACCURACY 32

#ifndef GAME_FPS
#define GAME_FPS 60
#endif

#if GAME_FPS < 30
#define ANIM_SPEED 4
#else
#define ANIM_SPEED 6
#endif

#if GAME_FPS > 30
#define PRE_GRAVITY SUBPIXELUNIT_ACCURACY >> 2
#else
#define PRE_GRAVITY SUBPIXELUNIT_ACCURACY >> 1
#endif

#define GROUNDLEVEL 200

#ifndef WIDTH
#define WIDTH 480
#endif

#ifndef HEIGHT
#define HEIGHT 270
#endif

// Gesture interactions with INTERTYPE_GESTUREs
// Basically flavor text
#define GESTURE_INVALID "There's nothing interesting here."
#define GESTURE_BED "Most people wouldn't want to sleep on a piled up blanket. I do, though."
#define GESTURE_COMPUTER "I don't really do much with this. Most of the stuff I see on the... \"Internet\" is just mindless political blabber."
#define GESTURE_BADGE "Aha, my Limetown Hospital badge! Sometimes I wonder why I feel such a strong connection with this thing."
#define GESTURE_KNIFE "Some blades are meant for precision. This is more like a weapon."
#define GESTURE_DONUT "What is a donut doing here? It may not be safe to eat, considering I have no clue how long it's been sitting here."
#define GESTURE_MIRROR "Now I can look into my eyes, my soul, and see just how cracked it is. Maybe even something behind me..."
#define GESTURE_YOU "This guy gives me some serious... uh... I'm not even sure how to describe it. I just feel very weird around him."
#define GESTURE_STOOL "Now where could the bartender be?" // may not be used, Gesturing with a stool can let you sit on it
#define GESTURE_RECEPTIONIST "God, I hate the world I live in... If only my psychiatry experience could help me figure out why I do." // receptionist desk
#define GESTURE_OUTDOORS "Why is the sky red? It's not even dawn."
#define GESTURE_BURNING "..."
#define GESTURE_GASOLINE "It isn't hard to put two and two together here now."
