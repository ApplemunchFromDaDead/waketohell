// Config file for Wake to Hell
// See constants.h for macros and stuff

// Default settings for the game to fall back to. Also applies when
// the options menu is inaccessible.
#define DEFAULT_OPTIONS (WTHOPTS_DOSONG | WTHOPTS_2XSIZE | WTHOPTS_FULLSC)
